package com.bstu.brest.coursework.types;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a x of double numbers.
 * It is a parent class for Layer, Filter and Map classes.
 */
public class DoubleSetImpl implements DoubleSet {

    /**
     * Size of n-dimensional array x.
     */
    protected int[] size;

    private final int[] coordinates;

    private final int numOfDimensions;

    private final int count;

    @Override
    public int[] getSize() {
        return size;
    }

    @Override
    public int getNumOfDimensions() {
        return numOfDimensions;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public int[] getCoordinates() {
        return coordinates;
    }

    /**
     * n-dimensional array, which is stored in one-dimensional array.
     */
    private double[][][][] x;

    public DoubleSetImpl(DoubleSet oldDoubleSet) {
        this.numOfDimensions = oldDoubleSet.getNumOfDimensions();
        this.count = oldDoubleSet.getCount();
        this.coordinates = oldDoubleSet.getCoordinates();
        this.size = oldDoubleSet.getSize();
        DoubleSetImpl oldDs = (DoubleSetImpl) oldDoubleSet;
        this.x = oldDs.x;
    }

    /**
     * Constructor.
     *
     * @param args - size of n-dimensional array.
     */
    public DoubleSetImpl(int... args) {
        if (args.length == 0 || args.length > 4) {
            throw new IllegalArgumentException(
                    "Dimension of DoubleSet should be equal to 1, 2, 3 or 4.");
        }
        int count = 1;
        for (int arg : args) {
            if (arg <= 0) {
                throw new NegativeArraySizeException(
                        "Dimensions of DoubleSet should be positive numbers.");
            }
            count *= arg;
        }
        this.count = count;
        numOfDimensions = args.length;
        size = args;
        coordinates = new int[numOfDimensions];
        switch (numOfDimensions) {
            case 1:
                x = new double[1][1][1][size[0]];
                break;
            case 2:
                x = new double[1][1][size[0]][size[1]];
                break;
            case 3:
                x = new double[1][size[0]][size[1]][size[2]];
                break;
            case 4:
                x = new double[size[0]][size[1]][size[2]][size[3]];
                break;
        }
    }

    @Override
    public void setSmallerDoubleSetByIndex(int index, DoubleSet smallerDoubleSet) {
        if (this.numOfDimensions - smallerDoubleSet.getNumOfDimensions() != 1) {
            throw new IllegalArgumentException(
                    "this.numOfDimensions - smallerDoubleSet.numOfDimensions != 1");
        }
        DoubleSetImpl dsImpl = (DoubleSetImpl) smallerDoubleSet;
        switch (this.numOfDimensions) {
            case 2:
                this.x[0][0][index] = dsImpl.x[0][0][0];
                break;
            case 3:
                this.x[0][index] = dsImpl.x[0][0];
                break;
            case 4:
                this.x[index] = dsImpl.x[0];
                break;
        }
    }

    @Override
    public DoubleSetImpl getSmallerDoubleSetByIndex(int index) {
        if (this.numOfDimensions < 2) {
            throw new UnsupportedOperationException(
                    "DoubleSet: getSmallerDoubleSetByIndex: this.numOfDimensions < 2");
        }
        int[] newSize = new int[this.numOfDimensions - 1];
        if (size.length - 1 >= 0) System.arraycopy(size, 1, newSize, 0, size.length - 1);
        DoubleSetImpl smallerDoubleSet = new DoubleSetImpl(newSize);
        switch (this.numOfDimensions) {
            case 2:
                smallerDoubleSet.x[0][0][0] = this.x[0][0][index];
                break;
            case 3:
                smallerDoubleSet.x[0][0] = this.x[0][index];
                break;
            case 4:
                smallerDoubleSet.x[0] = this.x[index];
                break;
        }
        return smallerDoubleSet;
    }

    /**
     * Getter.
     *
     * @param args - indexes.
     * @return - found element.
     */
    @Override
    public double get(int... args) {
        if (args.length != numOfDimensions) {
            throw new IllegalArgumentException("GetX has wrong number of params.");
        }
        switch (args.length) {
            case 1:
                return x[0][0][0][args[0]];
            case 2:
                return x[0][0][args[0]][args[1]];
            case 3:
                return x[0][args[0]][args[1]][args[2]];
            default:
                return x[args[0]][args[1]][args[2]][args[3]];
        }
    }

    /**
     * Setter.
     *
     * @param value - value.
     * @param args  - indexes.
     */
    @Override
    public void set(double value, int... args) {
        if (args.length != numOfDimensions) {
            throw new IllegalArgumentException("SetX should get from 1 to 4 params.");
        }
        switch (args.length) {
            case 1:
                x[0][0][0][args[0]] = value;
                break;
            case 2:
                x[0][0][args[0]][args[1]] = value;
                break;
            case 3:
                x[0][args[0]][args[1]][args[2]] = value;
                break;
            default:
                x[args[0]][args[1]][args[2]][args[3]] = value;
                break;
        }
    }

    @Override
    public void setBySingleIndex(double value, int index) {
        this.set(value, getRealCoordinates(index));
    }

    @Override
    public double getBySingleIndex(int index) {
        return this.get(getRealCoordinates(index));
    }

    /**
     * Appends value to the array's element.
     *
     * @param value - value.
     * @param args  - indexes.
     */
    @Override
    public void append(double value, int... args) {
        if (args.length != numOfDimensions) {
            throw new IllegalArgumentException("AppendX should get from 1 to 4 params.");
        }
        switch (args.length) {
            case 1:
                x[0][0][0][args[0]] += value;
                break;
            case 2:
                x[0][0][args[0]][args[1]] += value;
                break;
            case 3:
                x[0][args[0]][args[1]][args[2]] += value;
                break;
            default:
                x[args[0]][args[1]][args[2]][args[3]] += value;
                break;
        }
    }

    /**
     * Gets real coordinates by index in the one-dimensional array.
     *
     * @param index - index.
     * @return coordinates.
     */
    private int[] getRealCoordinates(int index) {
        switch (numOfDimensions) {
            case 1:
                coordinates[0] = index;
                break;
            case 2:
                coordinates[0] = index / size[1];
                coordinates[1] = index % size[1];
                break;
            case 3:
                coordinates[0] = index / (size[1] * size[2]);
                int temp = index % (size[1] * size[2]);
                coordinates[1] = temp / size[2];
                coordinates[2] = temp % size[2];
                break;
            case 4:
                coordinates[0] = index / (size[1] * size[2] * size[3]);
                int temp1 = index % (size[1] * size[2] * size[3]);
                coordinates[1] = temp1 / (size[2] * size[3]);
                int temp2 = temp1 % (size[2] * size[3]);
                coordinates[2] = temp2 / size[3];
                coordinates[3] = temp2 % size[3];
                break;
        }
        return coordinates;
    }

    public void setZeros() {
        for (double[][][] arr3 : x) {
            for (double[][] arr2 : arr3) {
                for (double[] arr1 : arr2) {
                    Arrays.fill(arr1, 0.);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoubleSetImpl doubleSet = (DoubleSetImpl) o;
        return numOfDimensions == doubleSet.numOfDimensions &&
               count == doubleSet.count &&
               Arrays.equals(size, doubleSet.size) &&
               Arrays.equals(coordinates, doubleSet.coordinates) &&
               Arrays.equals(x, doubleSet.x);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(numOfDimensions, count);
        result = 31 * result + Arrays.hashCode(size);
        result = 31 * result + Arrays.hashCode(coordinates);
        result = 31 * result + Arrays.hashCode(x);
        return result;
    }
}
