package com.bstu.brest.coursework.types;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class BatchData {

    public BatchData(ArrayList<ImageData> batchImages) {
        int[] dims = batchImages.get(0).getImage().getSize();


//        int dim2 = batchImages.get(0).getImage().getSize()[0];
//        int dim3 = batchImages.get(0).getImage().getSize()[1];
//        int dim4 = batchImages.get(0).getImage().getSize()[2];
        int lblDim = batchImages.get(0).getLabel().getSize()[0];
//        this.images = new DoubleSet(batchImages.size(), dim2, dim3, dim4);
        int[] newDims = new int[dims.length + 1];
        newDims[0] = batchImages.size();
        System.arraycopy(dims, 0, newDims, 1, dims.length);
        this.images = new DoubleSetImpl(newDims);

        this.labels = new DoubleSetImpl(batchImages.size(), lblDim);
        this.labelNumbers = new ShortSet(batchImages.size());
        for (int i = 0; i < batchImages.size(); i++) {
            ImageData imageData = batchImages.get(i);
            images.setSmallerDoubleSetByIndex(i, imageData.getImage());
            labels.setSmallerDoubleSetByIndex(i, imageData.getLabel());
            labelNumbers.set(imageData.getLabelNumber(), i);
        }
    }

    public BatchData(DoubleSet images, DoubleSet labels, ShortSet labelNumbers) {
        this.images = images;
        this.labels = labels;
        this.labelNumbers = labelNumbers;
    }

    private final DoubleSet images;

    private final DoubleSet labels;

    private final ShortSet labelNumbers;

    public DoubleSet getImages() {
        return images;
    }

    public DoubleSet getLabels() {
        return labels;
    }

    public ShortSet getLabelNumbers() {
        return labelNumbers;
    }

    public void drawImage(int index, BufferedWriter wResults, DoubleSet lastLayer)
            throws IOException {
        printProbability(index, wResults, lastLayer);
        for (int q = 0; q < 1; q++) {
            for (int j = 0; j < 28; j++) {
                for (int l = 0; l < 28; l++) {
                    wResults.write(
                            String.format("%3d ", Math.round(images.get(index, q, j, l) * 255.)));
                }
                wResults.write("\n");
            }
        }
        wResults.write("\n");
    }

    private void printProbability(int index, BufferedWriter wResults, DoubleSet lastLayer)
            throws IOException {
        double sum = 0.;
        for (int i = 0; i < 10; i++) {
            sum += Math.exp(lastLayer.get(index, i));
        }
        for (int i = 0; i < 10; i++) {
            wResults.write(i
                           + ": "
                           + (labels.get(index, i) == 1 ? "100%" : "0%")
                           + " "
                           + BigDecimal.valueOf(Math.exp(lastLayer.get(index, i)) / sum * 100.)
                                   .setScale(2, RoundingMode.HALF_UP).doubleValue()
                           + "%\n");
        }
        wResults.write("\n");
    }

    public ImageData getImageDatumByIndex(int i) {
        DoubleSet image = images.getSmallerDoubleSetByIndex(i);
        DoubleSet label = labels.getSmallerDoubleSetByIndex(i);
        short labelNumber = labelNumbers.get(i);
        return new ImageData(image, label, labelNumber);
    }
}
