package com.bstu.brest.coursework.types;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class ImageData {

    public ImageData(DoubleSet image, DoubleSet label) {
        this(image, label, (short) -1);
    }

    public ImageData(DoubleSet image, DoubleSet label, short labelNumber) {
        this.image = image;
        this.label = label;
        this.labelNumber = labelNumber;
    }

    private final DoubleSet image;

    private final DoubleSet label;

    private final short labelNumber;

    DoubleSet getImage() {
        return image;
    }

    DoubleSet getLabel() {
        return label;
    }

    short getLabelNumber() {
        return labelNumber;
    }

    public void drawImage(BufferedWriter wResults, DoubleSet lastLayer)
            throws IOException {
        printProbability(wResults, lastLayer);
        for (int q = 0; q < 1; q++) {
            for (int j = 0; j < 28; j++) {
                for (int l = 0; l < 28; l++) {
                    wResults.write(
                            String.format("%3d ", Math.round(image.get(q, j, l) * 255.)));
                }
                wResults.write("\n");
            }
        }
        wResults.write("\n");
    }

    private void printPrediction(BufferedWriter wResults, DoubleSet lastLayer)
            throws IOException {
        for (int i = 0; i < 10; i++) {
            wResults.write(i + ": " + (int) label.get(i) + " " + lastLayer.get(i) + "\n");
        }
        wResults.write("\n");
    }

    private void printProbability(BufferedWriter wResults, DoubleSet lastLayer)
            throws IOException {
        double sum = 0.;
        for (int i = 0; i < 10; i++) {
            sum += Math.exp(lastLayer.get(i));
        }
        for (int i = 0; i < 10; i++) {
            wResults.write(i
                           + ": "
                           + (label.get(i) == 1 ? "100%" : "0%")
                           + " "
                           + BigDecimal.valueOf(Math.exp(lastLayer.get(i)) / sum * 100.)
                                   .setScale(2, RoundingMode.HALF_UP).doubleValue()
                           + "%\n");
        }
        wResults.write("\n");
    }
}
