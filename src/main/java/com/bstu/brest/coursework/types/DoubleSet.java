package com.bstu.brest.coursework.types;

public interface DoubleSet {

    int[] getSize();

    int[] getCoordinates();

    int getNumOfDimensions();

    int getCount();

    void setSmallerDoubleSetByIndex(int index, DoubleSet smallerDoubleSet);

    DoubleSetImpl getSmallerDoubleSetByIndex(int index);

    /**
     * Getter.
     *
     * @param args - indexes.
     * @return - found element.
     */
    double get(int... args);

    /**
     * Setter.
     *
     * @param value - value.
     * @param args  - indexes.
     */
    void set(double value, int... args);

    void setBySingleIndex(double value, int index);

    double getBySingleIndex(int index);

    /**
     * Appends value to the array's element.
     *
     * @param value - value.
     * @param args  - indexes.
     */
    void append(double value, int... args);

    void setZeros();
}
