package com.bstu.brest.coursework.types;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a x of short numbers.
 * It is a parent class for Layer, Filter and Map classes.
 */
public class ShortSet {

    /**
     * n-dimensional array, which is stored in one-dimensional array.
     */
    private short[][][][] x;

    /**
     * Size of n-dimensional array x.
     */
    protected final int[] size;

    /**
     * Number of dimensions.
     */
    private final int numOfDimensions;

    /**
     * Count of elements.
     */
    private final int count;

    /**
     * Constructor.
     *
     * @param args - size of n-dimensional array.
     */
    public ShortSet(int... args) {
        if (args.length == 0 || args.length > 4) {
            throw new IllegalArgumentException(
                    "Dimension of ShortSet should be equal to 1, 2, 3 or 4.");
        }
        int count = 1;
        for (int arg : args) {
            if (arg <= 0) {
                throw new NegativeArraySizeException(
                        "Dimensions of ShortSet should be positive numbers.");
            }
            count *= arg;
        }
        this.count = count;
        numOfDimensions = args.length;
        size = args;
        switch (numOfDimensions) {
            case 1:
                x = new short[1][1][1][size[0]];
                break;
            case 2:
                x = new short[1][1][size[0]][size[1]];
                break;
            case 3:
                x = new short[1][size[0]][size[1]][size[2]];
                break;
            case 4:
                x = new short[size[0]][size[1]][size[2]][size[3]];
                break;
        }
    }

    /**
     * Getter.
     *
     * @param args - indexes.
     * @return - found element.
     */
    public short get(int... args) {
        if (args.length != numOfDimensions) {
            throw new IllegalArgumentException("GetX has wrong number of params.");
        }
        switch (args.length) {
            case 1:
                return x[0][0][0][args[0]];
            case 2:
                return x[0][0][args[0]][args[1]];
            case 3:
                return x[0][args[0]][args[1]][args[2]];
            default:
                return x[args[0]][args[1]][args[2]][args[3]];
        }
    }

    /**
     * Setter.
     *
     * @param value - value.
     * @param args  - indexes.
     */
    public void set(short value, int... args) {
        if (args.length != numOfDimensions) {
            throw new IllegalArgumentException("SetX should get from 1 to 4 params.");
        }
        switch (args.length) {
            case 1:
                x[0][0][0][args[0]] = value;
                break;
            case 2:
                x[0][0][args[0]][args[1]] = value;
                break;
            case 3:
                x[0][args[0]][args[1]][args[2]] = value;
                break;
            default:
                x[args[0]][args[1]][args[2]][args[3]] = value;
                break;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShortSet shortSet = (ShortSet) o;
        return numOfDimensions == shortSet.numOfDimensions &&
               count == shortSet.count &&
               Arrays.equals(x, shortSet.x) &&
               Arrays.equals(size, shortSet.size);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(numOfDimensions, count);
        result = 31 * result + Arrays.hashCode(x);
        result = 31 * result + Arrays.hashCode(size);
        return result;
    }
}
