package com.bstu.brest.coursework.apps.netreader;

import com.bstu.brest.coursework.types.BatchData;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.ImageData;

import java.util.ArrayList;

public interface NetReader {

    ArrayList<BatchData> getTrainingData();

    ArrayList<BatchData> getValidData();

    ArrayList<BatchData> getTestingData();

    DoubleSet getEtalonArr(int labelNumber);

    ArrayList<BatchData> imagesToBatches(ArrayList<ImageData> imageData);
}
