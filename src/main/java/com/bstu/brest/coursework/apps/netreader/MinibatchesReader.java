package com.bstu.brest.coursework.apps.netreader;

import com.bstu.brest.coursework.architecture.reader.FileReader;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;
import com.bstu.brest.coursework.types.ImageData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MinibatchesReader extends AbstractNetReader {

    /**
     * Testing file name for pictures.
     */
    private static final String PIC_FILE_NAME = "./src/main/resources/minibatchesImages.txt";

    private final short[] labelNumbers = {3, 0, 2, 4, 0, 1, 0, 2, 3, 4, 4, 1};

    private final int NUMBER_OF_LEARNING_IMAGES;

    private final int NUMBER_OF_VALID_IMAGES;

    private final int NUMBER_OF_TESTING_IMAGES;

    private final int PIC_NUMBER;

    private final int MINI_BATCH_SIZE;

    private ArrayList<ImageData> imageData;



    private void createBatches() {
        ArrayList<ImageData> trainingData = new ArrayList<>(
                imageData.subList(0, NUMBER_OF_LEARNING_IMAGES));
        ArrayList<ImageData> validData = new ArrayList<>(
                imageData.subList(NUMBER_OF_LEARNING_IMAGES,
                                  NUMBER_OF_LEARNING_IMAGES +
                                  NUMBER_OF_VALID_IMAGES));
        ArrayList<ImageData> testingData = new ArrayList<>(
                imageData.subList(NUMBER_OF_LEARNING_IMAGES + NUMBER_OF_VALID_IMAGES,
                                  NUMBER_OF_LEARNING_IMAGES +
                                  NUMBER_OF_VALID_IMAGES + NUMBER_OF_TESTING_IMAGES));
        System.out.println("Creating learning batches...");
        batchTrainingData = imagesToBatches(trainingData);
        System.out.println("Creating learning batches is finished.");
        System.out.println("Creating valid batches...");
        batchValidData = imagesToBatches(validData);
        System.out.println("Creating valid batches is finished.");
        System.out.println("Creating testing batches...");
        batchTestingData = imagesToBatches(testingData);
        System.out.println("Creating testing batches is finished.");
    }

    private void readArray() throws FileNotFoundException {
        System.out.println("Reading learning images...");
        imageData = new ArrayList<>(PIC_NUMBER);
        FileReader reader = new FileReader(PIC_FILE_NAME);
        for (int i = 0; i < PIC_NUMBER; i++) {
            short labelNumber = labelNumbers[i];
            DoubleSet label = getEtalonArr(labelNumber);
            DoubleSet image = new DoubleSetImpl(1, 7, 7);
            reader.read(image);
            ImageData imageDatum = new ImageData(image, label, labelNumber);
            imageData.add(imageDatum);
        }
        System.out.println("Reading is finished.");
    }

    /**
     * Constructor which creates new FileStream and BufferedReader.
     */
    public MinibatchesReader(int minibatchSize, int numberOfFeatures, int numOfLearningImages,
                               int numOfValidImages, int numOfTestingImages)
            throws IOException {
        super(numberOfFeatures, minibatchSize);

        if (numOfLearningImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistLearningReader(): numOfLearningImages % minibatchSize != 0");
        }
        if (numOfValidImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistLearningReader(): numOfValidImages % minibatchSize != 0");
        }

        this.NUMBER_OF_VALID_IMAGES = numOfValidImages;
        this.NUMBER_OF_LEARNING_IMAGES = numOfLearningImages;
        this.NUMBER_OF_TESTING_IMAGES = numOfTestingImages;
        this.PIC_NUMBER = numOfLearningImages + numOfValidImages + numOfTestingImages;
        this.MINI_BATCH_SIZE = minibatchSize;

        readArray();
//        Collections.shuffle(imageData);
        createBatches();
    }
}
