package com.bstu.brest.coursework.apps.netattacks.model;

import com.bstu.brest.coursework.architecture.reader.ArrayReader;
import com.bstu.brest.coursework.types.DoubleSetImpl;

import java.util.HashMap;
import java.util.Map;

public class NetworkConnectionModel {

    public NetworkConnectionModel(String row) {
        String[] arr = row.split(",");
        this.duration = Integer.parseInt(arr[0]);
        this.protocolType = ProtocolType.findByName(arr[1]);
        if (this.protocolType == null) {
            throw new NullPointerException("ProtocolType is null.");
        }
        this.service = Service.findByName(arr[2]);
        if (this.service == null) {
            throw new NullPointerException("Service is null.");
        }
        this.flag = Flag.findByName(arr[3]);
        if (this.flag == null) {
            throw new NullPointerException("Flag is null.");
        }
        this.srcBytes = Integer.parseInt(arr[4]);
        this.dstBytes = Integer.parseInt(arr[5]);
        this.land = Integer.parseInt(arr[6]) == 1;
        this.wrongFragment = Integer.parseInt(arr[7]);
        this.urgent = Integer.parseInt(arr[8]);
        this.hot = Integer.parseInt(arr[9]);
        this.numFailedLogins = Integer.parseInt(arr[10]);
        this.loggedIn = Integer.parseInt(arr[11]) == 1;
        this.numCompromised = Integer.parseInt(arr[12]);
        this.rootShell = Integer.parseInt(arr[13]) == 1;
        this.suAttempted = Integer.parseInt(arr[14]) == 1;
        this.numRoot = Integer.parseInt(arr[15]);
        this.numFileCreations = Integer.parseInt(arr[16]);
        this.numShells = Integer.parseInt(arr[17]);
        this.numAccessFiles = Integer.parseInt(arr[18]);
        this.numOutboundCmds = Integer.parseInt(arr[19]);
        this.isHostLogin = Integer.parseInt(arr[20]) == 1;
        this.isGuestLogin = Integer.parseInt(arr[21]) == 1;
        this.count = Integer.parseInt(arr[22]);
        this.srvCount = Integer.parseInt(arr[23]);
        this.serrorRate = Double.parseDouble(arr[24]);
        this.srvSerrorRate = Double.parseDouble(arr[25]);
        this.rerrorRate = Double.parseDouble(arr[26]);
        this.srvRerrorRate = Double.parseDouble(arr[27]);
        this.sameSrvRate = Double.parseDouble(arr[28]);
        this.diffSrvRate = Double.parseDouble(arr[29]);
        this.srvDiffHostRate = Double.parseDouble(arr[30]);
        this.dstHostCount = Integer.parseInt(arr[31]);
        this.dstHostSrvCount = Integer.parseInt(arr[32]);
        this.dstHostSameSrvRate = Double.parseDouble(arr[33]);
        this.dstHostDiffSrvRate = Double.parseDouble(arr[34]);
        this.dstHostSameSrcPortRate = Double.parseDouble(arr[35]);
        this.dstHostSrvDiffHostRate = Double.parseDouble(arr[36]);
        this.dstHostSerrorRate = Double.parseDouble(arr[37]);
        this.dstHostSrvSerrorRate = Double.parseDouble(arr[38]);
        this.dstHostRerrorRate = Double.parseDouble(arr[39]);
        this.dstHostSrvRerrorRate = Double.parseDouble(arr[40]);

        this.result = Result.findByName(arr[41].split("\\.")[0]);
        if (this.result == null) {
            throw new NullPointerException("Result is null.");
        }
    }

    public DoubleSetImpl toDoubleSet() {
        double[] array = new double[120];

        array[0] = this.duration / 42448.;

        array[1 - 1 + this.protocolType.getId()] = 1.;

        array[4 - 1 + this.service.getId()] = 1.;

        array[70 - 1 + this.flag.getId()] = 1.;

        array[83] = this.srcBytes / 5135678.;

        array[84] = this.dstBytes / 5153771.;

        array[85] = this.land ? 1. : 0.;

        array[86] = this.wrongFragment / 3.;

        array[87] = this.urgent / 3.;

        array[88] = this.hot / 30.;

        array[89] = this.numFailedLogins / 5.;

        array[90] = this.loggedIn ? 1. : 0.;

        array[91] = this.numCompromised / 767.;

        array[92] = this.rootShell ? 1. : 0.;

        array[93] = this.suAttempted ? 1. : 0.;

        array[94] = this.numRoot / 857.;

        array[95] = this.numFileCreations / 25.;

        array[96] = this.numShells / 2.;

        array[97] = this.numAccessFiles / 6.;

        array[98] = this.numOutboundCmds;

        array[99] = this.isHostLogin ? 1. : 0.;

        array[100] = this.isGuestLogin ? 1. : 0.;

        array[101] = this.count / 511.;

        array[102] = this.srvCount / 511.;

        array[103] = this.serrorRate;

        array[104] = this.srvSerrorRate;

        array[105] = this.rerrorRate;

        array[106] = this.srvRerrorRate;

        array[107] = this.sameSrvRate;

        array[108] = this.diffSrvRate;

        array[109] = this.srvDiffHostRate;

        array[110] = this.dstHostCount / 255.;

        array[111] = this.dstHostSrvCount / 255.;

        array[112] = this.dstHostSameSrvRate;

        array[113] = this.dstHostDiffSrvRate;

        array[114] = this.dstHostSameSrcPortRate;

        array[115] = this.dstHostSrvDiffHostRate;

        array[116] = this.dstHostSerrorRate;

        array[117] = this.dstHostSrvSerrorRate;

        array[118] = this.dstHostRerrorRate;

        array[119] = this.dstHostSrvRerrorRate;

        DoubleSetImpl doubleSet = new DoubleSetImpl(120);
        ArrayReader arrayReader = new ArrayReader(array);
        arrayReader.read(doubleSet);
        return doubleSet;
    }

    enum ProtocolType {
        TCP(1, "tcp"),
        UDP(2, "udp"),
        ICMP(3, "icmp");

        private int id;
        private String name;

        private static final Map<String, ProtocolType> map;
        static {
            map = new HashMap<>();
            for (ProtocolType v : ProtocolType.values()) {
                map.put(v.name, v);
            }
        }
        public static ProtocolType findByName(String i) {
            return map.get(i);
        }

        ProtocolType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    enum Service {
        DISCARD(1, "discard"),
        Z39_50(2, "Z39_50"),
        AUTH(3, "auth"),
        ECO_I(4, "eco_i"),
        SSH(5, "ssh"),
        ECHO(6, "echo"),
        NNTP(7, "nntp"),
        SUPDUP(8, "supdup"),
        CTF(9, "ctf"),
        LDAP(10, "ldap"),
        TELNET(11, "telnet"),
        TIM_I(12, "tim_i"),
        POP_2(13, "pop_2"),
        NETBIOS_SSN(14, "netbios_ssn"),
        POP_3(15, "pop_3"),
        GOPHER(16, "gopher"),
        TFTP_U(17, "tftp_u"),
        NTP_U(18, "ntp_u"),
        SYSTAT(19, "systat"),
        PRINTER(20, "printer"),
        MTP(21, "mtp"),
        HOSTNAMES(22, "hostnames"),
        RJE(23, "rje"),
        NNSP(24, "nnsp"),
        NETSTAT(25, "netstat"),
        URH_I(26, "urh_i"),
        VMNET(27, "vmnet"),
        SHELL(28, "shell"),
        COURIER(29, "courier"),
        DOMAIN(30, "domain"),
        NAME(31, "name"),
        HTTP(32, "http"),
        IMAP4(33, "imap4"),
        IRC(34, "IRC"),
        UUCP(35, "uucp"),
        URP_I(36, "urp_i"),
        DAYTIME(37, "daytime"),
        EXEC(38, "exec"),
        KSHELL(39, "kshell"),
        OTHER(40, "other"),
        PRIVATE(41, "private"),
        ECR_I(42, "ecr_i"),
        LINK(43, "link"),
        BGP(44, "bgp"),
        ISO_TSAP(45, "iso_tsap"),
        LOGIN(46, "login"),
        RED_I(47, "red_i"),
        UUCP_PATH(48, "uucp_path"),
        REMOTE_JOB(49, "remote_job"),
        PM_DUMP(50, "pm_dump"),
        WHOIS(51, "whois"),
        NETBIOS_DGM(52, "netbios_dgm"),
        FINGER(53, "finger"),
        KLOGIN(54, "klogin"),
        SMTP(55, "smtp"),
        FTP(56, "ftp"),
        NETBIOS_NS(57, "netbios_ns"),
        HTTP_443(58, "http_443"),
        DOMAIN_U(59, "domain_u"),
        CSNET_NS(60, "csnet_ns"),
        SQL_NET(61, "sql_net"),
        EFS(62, "efs"),
        FTP_DATA(63, "ftp_data"),
        SUNRPC(64, "sunrpc"),
        X11(65, "X11"),
        TIME(66, "time");

        private int id;
        private String name;

        private static final Map<String, Service> map;
        static {
            map = new HashMap<>();
            for (Service v : Service.values()) {
                map.put(v.name, v);
            }
        }
        public static Service findByName(String i) {
            return map.get(i);
        }

        Service(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    enum Flag {
        SF(1, "SF"),
        S0(2, "S0"),
        S1(3, "S1"),
        S2(4, "S2"),
        S3(5, "S3"),
        OTH(6, "OTH"),
        REJ(7, "REJ"),
        RSTO(8, "RSTO"),
        RSTOS0(9, "RSTOS0"),
        SH(10, "SH"),
        RSTRH(11, "RSTRH"),
        SHR(12, "SHR"),
        RSTR(13, "RSTR");

        private int id;
        private String name;

        Flag(int id, String name) {
            this.id = id;
            this.name = name;
        }

        private static final Map<String, Flag> map;
        static {
            map = new HashMap<>();
            for (Flag v : Flag.values()) {
                map.put(v.name, v);
            }
        }
        public static Flag findByName(String i) {
            return map.get(i);
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    public enum Result {
        NORMAL(1, "normal"),
        IMAP(2, "imap"),
        NMAP(3, "nmap"),
        ROOTKIT(4, "rootkit"),
        PORTSWEEP(5, "portsweep"),
        PERL(6, "perl"),
        IPSWEEP(7, "ipsweep"),
        MULTIHOP(8, "multihop"),
        SPY(9, "spy"),
        BUFFER_OVERFLOW(10, "buffer_overflow"),
        TEARDROP(11, "teardrop"),
        SMURF(12, "smurf"),
        WAREZCLIENT(13, "warezclient"),
        FTP_WRITE(14, "ftp_write"),
        GUESS_PASSWD(15, "guess_passwd"),
        SATAN(16, "satan"),
        NEPTUNE(17, "neptune"),
        PHF(18, "phf"),
        BACK(19, "back"),
        WAREZMASTER(20, "warezmaster"),
        LOADMODULE(21, "loadmodule"),
        LAND(22, "land"),
        POD(23, "pod");

        private int id;
        private String name;

        Result(int id, String name) {
            this.id = id;
            this.name = name;
        }

        private static final Map<String, Result> map;
        static {
            map = new HashMap<>();
            for (Result v : Result.values()) {
                map.put(v.name, v);
            }
        }
        public static Result findByName(String i) {
            return map.get(i);
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

    private int duration;
    private ProtocolType protocolType;
    private Service service;
    private Flag flag;
    private int srcBytes;
    private int dstBytes;
    private boolean land;
    private int wrongFragment;
    private int urgent;
    private int hot;
    private int numFailedLogins;
    private boolean loggedIn;
    private int numCompromised;
    private boolean rootShell;
    private boolean suAttempted;
    private int numRoot;
    private int numFileCreations;
    private int numShells;
    private int numAccessFiles;
    private int numOutboundCmds;
    private boolean isHostLogin;
    private boolean isGuestLogin;
    private int count;
    private int srvCount;
    private double serrorRate;
    private double srvSerrorRate;
    private double rerrorRate;
    private double srvRerrorRate;
    private double sameSrvRate;
    private double diffSrvRate;
    private double srvDiffHostRate;
    private int dstHostCount;
    private int dstHostSrvCount;
    private double dstHostSameSrvRate;
    private double dstHostDiffSrvRate;
    private double dstHostSameSrcPortRate;
    private double dstHostSrvDiffHostRate;
    private double dstHostSerrorRate;
    private double dstHostSrvSerrorRate;
    private double dstHostRerrorRate;
    private double dstHostSrvRerrorRate;

    private Result result;

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public ProtocolType getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(
            ProtocolType protocolType) {
        this.protocolType = protocolType;
    }

    public Service getService() {
        return service;
    }

    public void setService(
            Service service) {
        this.service = service;
    }

    public Flag getFlag() {
        return flag;
    }

    public void setFlag(
            Flag flag) {
        this.flag = flag;
    }

    public int getSrcBytes() {
        return srcBytes;
    }

    public void setSrcBytes(int srcBytes) {
        this.srcBytes = srcBytes;
    }

    public int getDstBytes() {
        return dstBytes;
    }

    public void setDstBytes(int dstBytes) {
        this.dstBytes = dstBytes;
    }

    public boolean isLand() {
        return land;
    }

    public void setLand(boolean land) {
        this.land = land;
    }

    public int getWrongFragment() {
        return wrongFragment;
    }

    public void setWrongFragment(int wrongFragment) {
        this.wrongFragment = wrongFragment;
    }

    public int getUrgent() {
        return urgent;
    }

    public void setUrgent(int urgent) {
        this.urgent = urgent;
    }

    public int getHot() {
        return hot;
    }

    public void setHot(int hot) {
        this.hot = hot;
    }

    public int getNumFailedLogins() {
        return numFailedLogins;
    }

    public void setNumFailedLogins(int numFailedLogins) {
        this.numFailedLogins = numFailedLogins;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public int getNumCompromised() {
        return numCompromised;
    }

    public void setNumCompromised(int numCompromised) {
        this.numCompromised = numCompromised;
    }

    public boolean isRootShell() {
        return rootShell;
    }

    public void setRootShell(boolean rootShell) {
        this.rootShell = rootShell;
    }

    public boolean isSuAttempted() {
        return suAttempted;
    }

    public void setSuAttempted(boolean suAttempted) {
        this.suAttempted = suAttempted;
    }

    public int getNumRoot() {
        return numRoot;
    }

    public void setNumRoot(int numRoot) {
        this.numRoot = numRoot;
    }

    public int getNumFileCreations() {
        return numFileCreations;
    }

    public void setNumFileCreations(int numFileCreations) {
        this.numFileCreations = numFileCreations;
    }

    public int getNumShells() {
        return numShells;
    }

    public void setNumShells(int numShells) {
        this.numShells = numShells;
    }

    public int getNumAccessFiles() {
        return numAccessFiles;
    }

    public void setNumAccessFiles(int numAccessFiles) {
        this.numAccessFiles = numAccessFiles;
    }

    public int getNumOutboundCmds() {
        return numOutboundCmds;
    }

    public void setNumOutboundCmds(int numOutboundCmds) {
        this.numOutboundCmds = numOutboundCmds;
    }

    public boolean isHostLogin() {
        return isHostLogin;
    }

    public void setHostLogin(boolean hostLogin) {
        isHostLogin = hostLogin;
    }

    public boolean isGuestLogin() {
        return isGuestLogin;
    }

    public void setGuestLogin(boolean guestLogin) {
        isGuestLogin = guestLogin;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSrvCount() {
        return srvCount;
    }

    public void setSrvCount(int srvCount) {
        this.srvCount = srvCount;
    }

    public double getSerrorRate() {
        return serrorRate;
    }

    public void setSerrorRate(double serrorRate) {
        this.serrorRate = serrorRate;
    }

    public double getSrvSerrorRate() {
        return srvSerrorRate;
    }

    public void setSrvSerrorRate(double srvSerrorRate) {
        this.srvSerrorRate = srvSerrorRate;
    }

    public double getRerrorRate() {
        return rerrorRate;
    }

    public void setRerrorRate(double rerrorRate) {
        this.rerrorRate = rerrorRate;
    }

    public double getSrvRerrorRate() {
        return srvRerrorRate;
    }

    public void setSrvRerrorRate(double srvRerrorRate) {
        this.srvRerrorRate = srvRerrorRate;
    }

    public double getSameSrvRate() {
        return sameSrvRate;
    }

    public void setSameSrvRate(double sameSrvRate) {
        this.sameSrvRate = sameSrvRate;
    }

    public double getDiffSrvRate() {
        return diffSrvRate;
    }

    public void setDiffSrvRate(double diffSrvRate) {
        this.diffSrvRate = diffSrvRate;
    }

    public double getSrvDiffHostRate() {
        return srvDiffHostRate;
    }

    public void setSrvDiffHostRate(double srvDiffHostRate) {
        this.srvDiffHostRate = srvDiffHostRate;
    }

    public int getDstHostCount() {
        return dstHostCount;
    }

    public void setDstHostCount(int dstHostCount) {
        this.dstHostCount = dstHostCount;
    }

    public int getDstHostSrvCount() {
        return dstHostSrvCount;
    }

    public void setDstHostSrvCount(int dstHostSrvCount) {
        this.dstHostSrvCount = dstHostSrvCount;
    }

    public double getDstHostSameSrvRate() {
        return dstHostSameSrvRate;
    }

    public void setDstHostSameSrvRate(double dstHostSameSrvRate) {
        this.dstHostSameSrvRate = dstHostSameSrvRate;
    }

    public double getDstHostDiffSrvRate() {
        return dstHostDiffSrvRate;
    }

    public void setDstHostDiffSrvRate(double dstHostDiffSrvRate) {
        this.dstHostDiffSrvRate = dstHostDiffSrvRate;
    }

    public double getDstHostSameSrcPortRate() {
        return dstHostSameSrcPortRate;
    }

    public void setDstHostSameSrcPortRate(double dstHostSameSrcPortRate) {
        this.dstHostSameSrcPortRate = dstHostSameSrcPortRate;
    }

    public double getDstHostSrvDiffHostRate() {
        return dstHostSrvDiffHostRate;
    }

    public void setDstHostSrvDiffHostRate(double dstHostSrvDiffHostRate) {
        this.dstHostSrvDiffHostRate = dstHostSrvDiffHostRate;
    }

    public double getDstHostSerrorRate() {
        return dstHostSerrorRate;
    }

    public void setDstHostSerrorRate(double dstHostSerrorRate) {
        this.dstHostSerrorRate = dstHostSerrorRate;
    }

    public double getDstHostSrvSerrorRate() {
        return dstHostSrvSerrorRate;
    }

    public void setDstHostSrvSerrorRate(double dstHostSrvSerrorRate) {
        this.dstHostSrvSerrorRate = dstHostSrvSerrorRate;
    }

    public double getDstHostRerrorRate() {
        return dstHostRerrorRate;
    }

    public void setDstHostRerrorRate(double dstHostRerrorRate) {
        this.dstHostRerrorRate = dstHostRerrorRate;
    }

    public double getDstHostSrvRerrorRate() {
        return dstHostSrvRerrorRate;
    }

    public void setDstHostSrvRerrorRate(double dstHostSrvRerrorRate) {
        this.dstHostSrvRerrorRate = dstHostSrvRerrorRate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
