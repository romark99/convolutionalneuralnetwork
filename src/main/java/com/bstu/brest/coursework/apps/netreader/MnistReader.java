package com.bstu.brest.coursework.apps.netreader;

import com.bstu.brest.coursework.types.BatchData;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;
import com.bstu.brest.coursework.types.ImageData;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;

public class MnistReader  extends AbstractNetReader {

    /**
     * Magic numbers for MNIST.
     */
    private static final int LBL_MAGIC_NUMBER = 2049;
    private static final int PIC_MAGIC_NUMBER = 2051;

    /**
     * Testing file name for pictures.
     */
    private static final String PIC_FILE_NAME = "./src/main/resources/train-images-idx3" +
                                                "-ubyte";

    /**
     * Testing file name for labels.
     */
    private static final String LBL_FILE_NAME = "./src/main/resources/train-labels-idx1" +
                                                "-ubyte";

    /**
     * Testing file name for pictures.
     */
    private static final String TESTING_PIC_FILE_NAME = "./src/main/resources/t10k-images-idx3" +
                                                        "-ubyte";

    /**
     * Testing file name for labels.
     */
    private static final String TESTING_LBL_FILE_NAME = "./src/main/resources/t10k-labels-idx1" +
                                                        "-ubyte";

    private final int NUMBER_OF_LEARNING_IMAGES;

    private final int NUMBER_OF_VALID_IMAGES;

    private final int NUMBER_OF_TESTING_IMAGES;

    private final int PIC_NUMBER;

    /**
     * Buffered stream for pictures.
     */
    private BufferedInputStream picBufferedStream;

    /**
     * Buffered stream for labels.
     */
    private BufferedInputStream lblBufferedStream;

    private BufferedInputStream testPicBufferedStream;

    private BufferedInputStream testLblBufferedStream;

    private ArrayList<ImageData> imageData;

    private ArrayList<ImageData> trainingData;

    private ArrayList<ImageData> testingData;

    private int padding;

    @Override
    public ArrayList<BatchData> getTrainingData() {
        Collections.shuffle(trainingData);
        batchTrainingData = imagesToBatches(trainingData);
        return batchTrainingData;
    }

    private void createBatches() {
        trainingData = new ArrayList<>(
                imageData.subList(0, NUMBER_OF_LEARNING_IMAGES));
        ArrayList<ImageData> validData = new ArrayList<>(
                imageData.subList(NUMBER_OF_LEARNING_IMAGES,
                                  NUMBER_OF_LEARNING_IMAGES +
                                  NUMBER_OF_VALID_IMAGES));
        System.out.println("Creating learning batches...");
        batchTrainingData = imagesToBatches(trainingData);
        System.out.println("Creating learning batches is finished.");
        System.out.println("Creating valid batches...");
        batchValidData = imagesToBatches(validData);
        System.out.println("Creating valid batches is finished.");
        System.out.println("Creating testing batches...");
        batchTestingData = imagesToBatches(testingData);
        System.out.println("Creating testing batches is finished.");
    }

    private void readArray() throws IOException {
        System.out.println("Reading learning images...");
        imageData = new ArrayList<>(PIC_NUMBER);
        for (int i = 0; i < PIC_NUMBER; i++) {
            short labelNumber = (short) lblBufferedStream.read();
            DoubleSet label = getEtalonArr(labelNumber);
            DoubleSet image = new DoubleSetImpl(1, 28 + 2 * padding, 28 + 2 * padding);
            for (int j = 0; j < 28; j++) {
                for (int k = 0; k < 28; k++) {
                    image.set(picBufferedStream.read() / 255., 0, j + padding,
                              k + padding);
                }
            }
            ImageData imageDatum = new ImageData(image, label, labelNumber);
            imageData.add(imageDatum);
        }
        System.out.println("Reading is finished.");
        System.out.println("Reading testing images...");
        testingData = new ArrayList<>(NUMBER_OF_TESTING_IMAGES);
        for (int i = 0; i < NUMBER_OF_TESTING_IMAGES; i++) {
            short labelNumber = (short) testLblBufferedStream.read();
            DoubleSet label = getEtalonArr(labelNumber);
            DoubleSet image = new DoubleSetImpl(1, 28 + 2 * padding, 28 + 2 * padding);
            for (int j = 0; j < 28; j++) {
                for (int k = 0; k < 28; k++) {
                    image.set(testPicBufferedStream.read() / 255., 0, j + padding,
                              k + padding);
                }
            }
            ImageData imageDatum = new ImageData(image, label, labelNumber);
            testingData.add(imageDatum);
        }
        System.out.println("Reading is finished.");
    }

    /**
     * Constructor which creates new FileStream and BufferedReader.
     */
    public MnistReader(int miniImageSize, int numberOfFeatures, int numOfLearningImages,
                       int numOfValidImages, int numOfTestingImages)
            throws IOException {
        this(miniImageSize, numberOfFeatures, numOfLearningImages, numOfValidImages, numOfTestingImages, 0);
    }

    /**
     * Constructor which creates new FileStream and BufferedReader.
     */
    public MnistReader(int minibatchSize, int numberOfFeatures, int numOfLearningImages,
                       int numOfValidImages, int numOfTestingImages, int padding)
            throws IOException {
        super(numberOfFeatures, minibatchSize);

        if (numOfLearningImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistReader(): numOfLearningImages % minibatchSize != 0");
        }
        if (numOfValidImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistReader(): numOfValidImages % minibatchSize != 0");
        }
        if (numOfTestingImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistReader(): numOfTestingImages % minibatchSize != 0");
        }
        FileInputStream lblFileStream = new FileInputStream(LBL_FILE_NAME);
        this.lblBufferedStream = new BufferedInputStream(lblFileStream);

        FileInputStream testLblFileStream = new FileInputStream(TESTING_LBL_FILE_NAME);
        this.testLblBufferedStream = new BufferedInputStream(testLblFileStream);

        this.NUMBER_OF_VALID_IMAGES = numOfValidImages;
        this.NUMBER_OF_LEARNING_IMAGES = numOfLearningImages;
        this.NUMBER_OF_TESTING_IMAGES = numOfTestingImages;
        this.PIC_NUMBER = numOfLearningImages + numOfValidImages;

        FileInputStream picFileStream = new FileInputStream(PIC_FILE_NAME);
        this.picBufferedStream = new BufferedInputStream(picFileStream);

        FileInputStream testPicFileStream = new FileInputStream(TESTING_PIC_FILE_NAME);
        this.testPicBufferedStream = new BufferedInputStream(testPicFileStream);

        this.padding = padding;

        ByteBuffer wrapped;
        byte[] bLblMagNumber = new byte[4];
        byte[] bPicMagNumber = new byte[4];
        byte[] bTLblMagNumber = new byte[4];
        byte[] bTPicMagNumber = new byte[4];
        for (int i = 0; i < 4; i++) {
            bLblMagNumber[i] = (byte) lblBufferedStream.read();
            bPicMagNumber[i] = (byte) picBufferedStream.read();
            bTLblMagNumber[i] = (byte) testLblBufferedStream.read();
            bTPicMagNumber[i] = (byte) testPicBufferedStream.read();
        }
        wrapped = ByteBuffer.wrap(bLblMagNumber);
        int lblMagNumber = wrapped.getInt();
        if (lblMagNumber != LBL_MAGIC_NUMBER) {
            throw new NumberFormatException(
                    "MnistReader: read(): magic number for label file is wrong.");
        }
        wrapped = ByteBuffer.wrap(bPicMagNumber);
        int picMagNumber = wrapped.getInt();
        if (picMagNumber != PIC_MAGIC_NUMBER) {
            throw new NumberFormatException(
                    "MnistReader: read(): magic number for pictures file is wrong.");
        }
        wrapped = ByteBuffer.wrap(bTLblMagNumber);
        int tLblMagNumber = wrapped.getInt();
        if (tLblMagNumber != LBL_MAGIC_NUMBER) {
            throw new NumberFormatException(
                    "MnistReader: read(): magic number for label file is wrong.");
        }
        wrapped = ByteBuffer.wrap(bTPicMagNumber);
        int tPicMagNumber = wrapped.getInt();
        if (tPicMagNumber != PIC_MAGIC_NUMBER) {
            throw new NumberFormatException(
                    "MnistReader: read(): magic number for pictures file is wrong.");
        }
        byte[] bLblNumber = new byte[4];
        byte[] bPicNumber = new byte[4];
        byte[] tBLblNumber = new byte[4];
        byte[] tBPicNumber = new byte[4];
        for (int i = 0; i < 4; i++) {
            bLblNumber[i] = (byte) lblBufferedStream.read();
            bPicNumber[i] = (byte) picBufferedStream.read();
            tBLblNumber[i] = (byte) testLblBufferedStream.read();
            tBPicNumber[i] = (byte) testPicBufferedStream.read();
        }
        wrapped = ByteBuffer.wrap(bLblNumber);
        int lblNumber = wrapped.getInt();
        wrapped = ByteBuffer.wrap(bPicNumber);
        int picNumber = wrapped.getInt();
        wrapped = ByteBuffer.wrap(tBLblNumber);
        int tLblNumber = wrapped.getInt();
        wrapped = ByteBuffer.wrap(tBPicNumber);
        int tPicNumber = wrapped.getInt();
        if (lblNumber != picNumber) {
            throw new NumberFormatException(
                    "MnistReader(): label and pictures files have a different number of" +
                    " records.");
        }
        if (tLblNumber != tPicNumber) {
            throw new NumberFormatException(
                    "MnistReader(): label and pictures files have a different number of" +
                    " records.");
        }
        byte[] bPicRows = new byte[4];
        byte[] bPicColumns = new byte[4];
        byte[] tBPicRows = new byte[4];
        byte[] tBPicColumns = new byte[4];
        for (int i = 0; i < 4; i++) {
            bPicRows[i] = (byte) picBufferedStream.read();
        }
        for (int i = 0; i < 4; i++) {
            bPicColumns[i] = (byte) picBufferedStream.read();
        }
        for (int i = 0; i < 4; i++) {
            tBPicRows[i] = (byte) testPicBufferedStream.read();
        }
        for (int i = 0; i < 4; i++) {
            tBPicColumns[i] = (byte) testPicBufferedStream.read();
        }
        wrapped = ByteBuffer.wrap(bPicRows);


        /**
         * Width of pictures.
         */
        int width = wrapped.getInt();

        wrapped = ByteBuffer.wrap(bPicColumns);

        /**
         * Height of pictures.
         */
        int height = wrapped.getInt();

        wrapped = ByteBuffer.wrap(tBPicRows);

        int tWidth = wrapped.getInt();

        wrapped = ByteBuffer.wrap(tBPicColumns);

        int tHeight = wrapped.getInt();

        readArray();
        Collections.shuffle(imageData);
        createBatches();
    }
}
