package com.bstu.brest.coursework.apps.testminibatches;

import com.bstu.brest.coursework.apps.AbstractApp;
import com.bstu.brest.coursework.apps.netreader.MinibatchesReader;
import com.bstu.brest.coursework.apps.netreader.NetReader;
import com.bstu.brest.coursework.architecture.connection.ConvolutionalConn3D;
import com.bstu.brest.coursework.architecture.connection.FullyConnectedConn;
import com.bstu.brest.coursework.architecture.connection.MapToFullyConn;
import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.functions.CostFunctions;
import com.bstu.brest.coursework.architecture.layer.MapLayer;
import com.bstu.brest.coursework.architecture.network.NetworkImpl;
import com.bstu.brest.coursework.architecture.reader.FileReader;

import java.io.IOException;

public class TestMinibatchesApp extends AbstractApp {

    private static final int NUMBER_OF_LEARNING_PICTURES = 12;
    private static final int NUMBER_OF_VALID_PICTURES = 0;
    private static final int NUMBER_OF_TESTING_PICTURES = 0;

    private static final int NUMBER_OF_EPOCHS = 1000; //1000

    private static final int MINI_BATCH_SIZE = 3;

    private static final int NUMBER_OF_FEATURES = 5;

    private static final double ALPHA = 0.01 / MINI_BATCH_SIZE; //0.00001

    private static final CostFunctions COST_FUNCTION = CostFunctions.CROSS_ENTROPY;

    private static final boolean IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE = false;

    private static final boolean IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE = false;

    private static final boolean IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS = false;

    private static final boolean IS_DISPLAY_PN_MATRIX = false;

    private static final String BAD_VALID_RESULTS_NAME = "badValid.txt";
    private static final String BAD_TEST_RESULTS_NAME = "badTest.txt";

    private static final String W_FILE_NAME = "w4.txt";
    private static final String T_FILE_NAME = "t4.txt";

    public TestMinibatchesApp() {
        super(NUMBER_OF_LEARNING_PICTURES, NUMBER_OF_VALID_PICTURES,
              NUMBER_OF_TESTING_PICTURES, NUMBER_OF_EPOCHS,
              IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE,
              IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE, IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS,
              IS_DISPLAY_PN_MATRIX, BAD_VALID_RESULTS_NAME, BAD_TEST_RESULTS_NAME,
              W_FILE_NAME, T_FILE_NAME);
    }

    @Override
    public void main(String[] args) throws IOException {

        FileReader readerW = new FileReader(
                "./src/main/resources/minibatchesTestW1.txt");
        FileReader readerT = new FileReader(
                "./src/main/resources/minibatchesTestT1.txt");

        NetworkImpl network = new NetworkImpl(this, COST_FUNCTION, MINI_BATCH_SIZE, ALPHA,
                                              readerW, readerT);

        network.addFirstLayer(new MapLayer(MINI_BATCH_SIZE, 1, 7, 7))
                .addConnectionAndLayer(
                        new ConvolutionalConn3D(ActivationFunctions.RELU, 1, 1),
                        MINI_BATCH_SIZE, 3, 5, 5)
                .addConnectionAndLayer(
                        new ConvolutionalConn3D(ActivationFunctions.RELU, 2, 2),
                        MINI_BATCH_SIZE, 5, 4, 4)
                .addConnectionAndLayer(
                        new MapToFullyConn())
                .addConnectionAndLayer(
                        new FullyConnectedConn(ActivationFunctions.SIGMOID),
                        MINI_BATCH_SIZE, NUMBER_OF_FEATURES);

        NetReader minibatchesReader = new MinibatchesReader(MINI_BATCH_SIZE,
                                                            NUMBER_OF_FEATURES,
                                                            NUMBER_OF_LEARNING_PICTURES,
                                                            NUMBER_OF_VALID_PICTURES,
                                                            NUMBER_OF_TESTING_PICTURES);
        train(network, minibatchesReader);
    }
}
