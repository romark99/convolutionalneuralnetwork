package com.bstu.brest.coursework.apps;

import com.bstu.brest.coursework.architecture.network.Network;
import com.bstu.brest.coursework.apps.netreader.NetReader;

public abstract class AbstractApp implements App {

    private final int NUMBER_OF_LEARNING_SAMPLES;
    private final int NUMBER_OF_VALID_SAMPLES;
    private final int NUMBER_OF_TESTING_SAMPLES;

    private final int NUMBER_OF_EPOCHS;

    private final boolean IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE;

    private final boolean IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE;

    private final boolean IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS;

    private final boolean IS_DISPLAY_PN_MATRIX;

    private final String BAD_VALID_RESULTS_NAME;
    private final String BAD_TEST_RESULTS_NAME;

    private final String W_FILE_NAME;
    private final String T_FILE_NAME;

    protected AbstractApp(int NUMBER_OF_LEARNING_SAMPLES, int NUMBER_OF_VALID_SAMPLES,
                int NUMBER_OF_TESTING_SAMPLES, int NUMBER_OF_EPOCHS,
                boolean IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE,
                boolean IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE,
                boolean IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS,
                boolean IS_DISPLAY_PN_MATRIX, String BAD_VALID_RESULTS_NAME,
                String BAD_TEST_RESULTS_NAME, String W_FILE_NAME,
                String T_FILE_NAME) {
        this.NUMBER_OF_LEARNING_SAMPLES = NUMBER_OF_LEARNING_SAMPLES;
        this.NUMBER_OF_VALID_SAMPLES = NUMBER_OF_VALID_SAMPLES;
        this.NUMBER_OF_TESTING_SAMPLES = NUMBER_OF_TESTING_SAMPLES;
        this.NUMBER_OF_EPOCHS = NUMBER_OF_EPOCHS;
        this.IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE =
                IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE;
        this.IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE = IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE;
        this.IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS = IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS;
        this.IS_DISPLAY_PN_MATRIX = IS_DISPLAY_PN_MATRIX;
        this.BAD_VALID_RESULTS_NAME = BAD_VALID_RESULTS_NAME;
        this.BAD_TEST_RESULTS_NAME = BAD_TEST_RESULTS_NAME;
        this.W_FILE_NAME = W_FILE_NAME;
        this.T_FILE_NAME = T_FILE_NAME;
    }

    @Override
    public void train(Network network, NetReader reader) {
        long startTime = System.currentTimeMillis();
        int maxValidResult = 0;
        short howManyBadResults = 0;
        int maxTestResult = 0;
        int bestEpochIndex = -1;
        for (int i = 0; i < NUMBER_OF_EPOCHS; i++) {
            network.learn(reader.getTrainingData(), i);
            if (NUMBER_OF_VALID_SAMPLES > 0) {
                int validResult = network.validPredict(reader.getValidData(),
                                                       IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE
                                                       ? BAD_VALID_RESULTS_NAME : null,
                                                       IS_DISPLAY_PN_MATRIX);
                System.out.printf("Epoch %d: V %d / %d\n", i, validResult,
                                  NUMBER_OF_VALID_SAMPLES);
                if (validResult >= maxValidResult) {
                    howManyBadResults = 0;
                    maxValidResult = validResult;

                } else {
                    howManyBadResults++;
                    if (howManyBadResults > 10) {
                        break;
                    }
                }
            }
            if (NUMBER_OF_TESTING_SAMPLES > 0) {
                int testResult = network.testPredict(reader.getTestingData(),
                                                     IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE
                                                     ? BAD_TEST_RESULTS_NAME : null,
                                                     IS_DISPLAY_PN_MATRIX);
                System.out.printf("Epoch %d: T %d / %d\n", i, testResult,
                                  NUMBER_OF_TESTING_SAMPLES);
                if (testResult > maxTestResult) {
                    maxTestResult = testResult;
                    bestEpochIndex = i;
                    if (IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS) {
                        network.saveWeightsAndThresholds(W_FILE_NAME, T_FILE_NAME);
                    }
                }
            }
        }
        long endTime = System.currentTimeMillis();
        network.displayFinalResults(endTime - startTime, maxValidResult, maxTestResult,
                                    bestEpochIndex);
    }

    @Override
    public int getNumberOfTestingSamples() {
        return NUMBER_OF_TESTING_SAMPLES;
    }

    @Override
    public int getNumberOfValidSamples() {
        return NUMBER_OF_VALID_SAMPLES;
    }

    @Override
    public int getNumberOfLearningSamples() {
        return NUMBER_OF_LEARNING_SAMPLES;
    }
}
