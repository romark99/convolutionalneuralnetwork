package com.bstu.brest.coursework.apps.netreader;

import com.bstu.brest.coursework.apps.netattacks.model.NetworkConnectionModel;
import com.bstu.brest.coursework.types.BatchData;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.ImageData;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class NetAttacksReader extends AbstractNetReader {

    /**
     * Testing file name for pictures.
     */
    private static final String PIC_FILE_NAME = "./src/main/resources/net_attacks" +
                                                "/kddcup.txt";


    private final int NUMBER_OF_LEARNING_SAMPLES;

    private final int NUMBER_OF_VALID_SAMPLES;

    private final int NUMBER_OF_TESTING_SAMPLES;

    private final int NUMBER_OF_ALL_SAMPLES;

    /**
     * Buffered stream for pictures.
     */
    private BufferedReader picBufferedReader;

    private ArrayList<ImageData> allData;

    private ArrayList<ImageData> trainingData;

    @Override
    public ArrayList<BatchData> getTrainingData() {
        Collections.shuffle(trainingData);
        batchTrainingData = imagesToBatches(trainingData);
        return batchTrainingData;
    }

    private void createBatches() {
        trainingData = new ArrayList<>(
                allData.subList(0, NUMBER_OF_LEARNING_SAMPLES));
        ArrayList<ImageData> validData = new ArrayList<>(
                allData.subList(NUMBER_OF_LEARNING_SAMPLES,
                                NUMBER_OF_LEARNING_SAMPLES +
                                NUMBER_OF_VALID_SAMPLES));
        ArrayList<ImageData> testingData = new ArrayList<>(
                allData.subList(NUMBER_OF_LEARNING_SAMPLES +
                                NUMBER_OF_VALID_SAMPLES,
                                NUMBER_OF_LEARNING_SAMPLES +
                                NUMBER_OF_VALID_SAMPLES +
                                NUMBER_OF_TESTING_SAMPLES));
        System.out.println("Creating learning batches...");
        batchTrainingData = imagesToBatches(trainingData);
        System.out.println("Creating learning batches is finished.");
        System.out.println("Creating valid batches...");
        batchValidData = imagesToBatches(validData);
        System.out.println("Creating valid batches is finished.");
        System.out.println("Creating testing batches...");
        batchTestingData = imagesToBatches(testingData);
        System.out.println("Creating testing batches is finished.");
    }

    private void readArray() throws IOException {
        System.out.println("Reading all samples...");
        allData = new ArrayList<>(NUMBER_OF_ALL_SAMPLES);
        String strLine;
        int n = 0;
        try {
            while ((strLine = picBufferedReader.readLine()) != null) {
                n++;
                NetworkConnectionModel networkConnectionModel = new NetworkConnectionModel(
                        strLine);
                DoubleSet sample = networkConnectionModel.toDoubleSet();
                short labelNumber = (short) (networkConnectionModel.getResult().getId() - 1);
                DoubleSet label = getEtalonArr(labelNumber);
                ImageData imageData = new ImageData(sample, label, labelNumber);
                allData.add(imageData);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("N = " + n);
        }
        System.out.println("Reading is finished.");
    }

    /**
     * Constructor which creates new FileStream and BufferedReader.
     */
    public NetAttacksReader(int minibatchSize, int numberOfFeatures,
                            int numOfLearningImages,
                            int numOfValidImages, int numOfTestingImages)
            throws IOException {
        super(numberOfFeatures, minibatchSize);

        if (numOfLearningImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistReader(): numOfLearningImages % minibatchSize != 0");
        }
        if (numOfValidImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistReader(): numOfValidImages % minibatchSize != 0");
        }
        if (numOfTestingImages % minibatchSize != 0) {
            throw new IllegalArgumentException(
                    "MnistReader(): numOfTestingImages % minibatchSize != 0");
        }
        this.NUMBER_OF_VALID_SAMPLES = numOfValidImages;
        this.NUMBER_OF_LEARNING_SAMPLES = numOfLearningImages;
        this.NUMBER_OF_TESTING_SAMPLES = numOfTestingImages;
        this.NUMBER_OF_ALL_SAMPLES = numOfLearningImages + numOfValidImages + numOfTestingImages;

        FileInputStream picFileStream = new FileInputStream(PIC_FILE_NAME);
        this.picBufferedReader = new BufferedReader(new InputStreamReader(picFileStream));

        readArray();
        Collections.shuffle(allData);
        createBatches();
    }
}
