package com.bstu.brest.coursework.apps;

import com.bstu.brest.coursework.architecture.network.Network;
import com.bstu.brest.coursework.apps.netreader.NetReader;

import java.io.IOException;

public interface App {
    void main(String[] args) throws IOException;

    int getNumberOfTestingSamples();

    int getNumberOfValidSamples();

    int getNumberOfLearningSamples();

    void train(Network network, NetReader reader);
}
