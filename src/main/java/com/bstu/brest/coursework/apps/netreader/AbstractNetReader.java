package com.bstu.brest.coursework.apps.netreader;

import com.bstu.brest.coursework.types.BatchData;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;
import com.bstu.brest.coursework.types.ImageData;

import java.util.ArrayList;

public abstract class AbstractNetReader implements NetReader {

    private final int NUMBER_OF_FEATURES;

    private final int MINI_BATCH_SIZE;

    ArrayList<BatchData> batchTrainingData;

    ArrayList<BatchData> batchValidData;

    ArrayList<BatchData> batchTestingData;

    public AbstractNetReader(int NUMBER_OF_FEATURES, int MINI_BATCH_SIZE) {
        this.NUMBER_OF_FEATURES = NUMBER_OF_FEATURES;
        this.MINI_BATCH_SIZE = MINI_BATCH_SIZE;
    }

    @Override
    public DoubleSet getEtalonArr(int labelNumber) {
        DoubleSet doubleSet = new DoubleSetImpl(NUMBER_OF_FEATURES);
        doubleSet.set(1., labelNumber);
        return doubleSet;
    }

    @Override
    public ArrayList<BatchData> imagesToBatches(ArrayList<ImageData> imageData) {
        if (imageData.size() % MINI_BATCH_SIZE != 0) {
            throw new IllegalArgumentException(
                    "imagesToBatches(): imageData.size() % MINI_BATCH_SIZE != 0");
        }
        int numOfBatches = imageData.size() / MINI_BATCH_SIZE;
        ArrayList<BatchData> miniBatches = new ArrayList<>(numOfBatches);
        ArrayList<ImageData> batchImages = new ArrayList<>(MINI_BATCH_SIZE);
        BatchData currentBatch;
        for (int i = 0; i < imageData.size(); i++) {
            batchImages.add(imageData.get(i));
            if ((i + 1) % MINI_BATCH_SIZE == 0) {
                currentBatch = new BatchData(batchImages);
                miniBatches.add(currentBatch);
                batchImages = new ArrayList<>(MINI_BATCH_SIZE);
            }
        }
        return miniBatches;
    }

    @Override
    public ArrayList<BatchData> getTestingData() {
        return batchTestingData;
    }

    @Override
    public ArrayList<BatchData> getTrainingData() {
        return batchTrainingData;
    }

    @Override
    public ArrayList<BatchData> getValidData() {
        return batchValidData;
    }
}
