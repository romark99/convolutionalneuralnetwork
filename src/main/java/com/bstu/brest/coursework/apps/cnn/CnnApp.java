package com.bstu.brest.coursework.apps.cnn;

import com.bstu.brest.coursework.apps.AbstractApp;
import com.bstu.brest.coursework.apps.netreader.MnistReader;
import com.bstu.brest.coursework.apps.netreader.NetReader;
import com.bstu.brest.coursework.architecture.connection.ConvolutionalConn2D;
import com.bstu.brest.coursework.architecture.connection.FullyConnectedConn;
import com.bstu.brest.coursework.architecture.connection.MapToFullyConn;
import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.functions.CostFunctions;
import com.bstu.brest.coursework.architecture.layer.MapLayer;
import com.bstu.brest.coursework.architecture.network.NetworkImpl;
import com.bstu.brest.coursework.architecture.reader.NormalReader;

import java.io.IOException;

public class CnnApp extends AbstractApp {

    // 60000
    private static final int NUMBER_OF_LEARNING_PICTURES = 50000;
    private static final int NUMBER_OF_VALID_PICTURES = 10000;

    // 10000
    private static final int NUMBER_OF_TESTING_PICTURES = 10000;

    private static final int NUMBER_OF_EPOCHS = 100; //1000

    // !!! 1, 10, 25
    private static final int MINI_BATCH_SIZE = 1;

    private static final int NUMBER_OF_FEATURES = 10;

    private static final double ALPHA = 0.1 / MINI_BATCH_SIZE; //0.00001

    private static final CostFunctions COST_FUNCTION = CostFunctions.CROSS_ENTROPY;

    private static final boolean IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE = false;

    private static final boolean IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE = false;

    private static final boolean IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS = false;

    private static final boolean IS_DISPLAY_PN_MATRIX = false;

    private static final String BAD_VALID_RESULTS_NAME = "badValid.txt";
    private static final String BAD_TEST_RESULTS_NAME = "badTest.txt";

    private static final String W_FILE_NAME = "w4.txt";
    private static final String T_FILE_NAME = "t4.txt";

    public CnnApp() {
        super(NUMBER_OF_LEARNING_PICTURES, NUMBER_OF_VALID_PICTURES,
              NUMBER_OF_TESTING_PICTURES, NUMBER_OF_EPOCHS, IS_TO_WRITE_BAD_VALID_RESULTS_TO_FILE,
              IS_TO_WRITE_BAD_TEST_RESULTS_TO_FILE, IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS,
              IS_DISPLAY_PN_MATRIX, BAD_VALID_RESULTS_NAME, BAD_TEST_RESULTS_NAME,
              W_FILE_NAME, T_FILE_NAME);
    }

    @Override
    public void main(String[] args) throws IOException {
        NormalReader readerW = new NormalReader(true);
        NormalReader readerT = new NormalReader();

//        FileReader readerW = new FileReader(
//                "./src/main/resources/normalWForTestLenet5.txt");
//        FileReader readerT = new FileReader(
//                "./src/main/resources/normalTForTestLenet5.txt");

        NetworkImpl network = new NetworkImpl(this, COST_FUNCTION, MINI_BATCH_SIZE, ALPHA,
                                              readerW, readerT);

//        network.addFirstLayer(new MapLayer(1, 28, 28))
//                .addConnectionAndLayer(
//                        new MapToFullyConn())
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 100)
//                .addConnectionAndLayer(
//                        new FullyCnnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10); // 97.89 T


//        network.addFirstLayer(new MapLayer(1, 28, 28))
//                .addConnectionAndLayer(
//                        new ConvolutionalConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 20, 24, 24)
//                .addConnectionAndLayer(
//                        new PoolingConn(), 20, 12, 12)
//                .addConnectionAndLayer(
//                        new MapToFullyConn())
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 100)
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10); // 99.04 V // 99.15 T

        network.addFirstLayer(new MapLayer(MINI_BATCH_SIZE, 1, 28, 28))
                .addConnectionAndLayer(
                        new ConvolutionalConn2D(ActivationFunctions.SIGMOID, 2, 2),
                        MINI_BATCH_SIZE, 20, 24, 24)
                .addConnectionAndLayer(
                        new ConvolutionalConn2D(ActivationFunctions.SIGMOID, 2, 2),
                        MINI_BATCH_SIZE, 40, 8, 8)
                .addConnectionAndLayer(
                        new MapToFullyConn())
                .addConnectionAndLayer(
                        new FullyConnectedConn(ActivationFunctions.SIGMOID),
                        MINI_BATCH_SIZE, 100)
                .addConnectionAndLayer(
                        new FullyConnectedConn(ActivationFunctions.SIGMOID),
                        MINI_BATCH_SIZE, NUMBER_OF_FEATURES); //99.26 V // 99.28 T

//        network.addFirstLayer(new MapLayer(1, 28, 28))
//                .addConnectionAndLayer(
//                        new ConvolutionalConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 4, 24, 24)
//                .addConnectionAndLayer(
//                        new PoolingConn(), 4, 12, 12)
//                .addConnectionAndLayer(
//                        new ConvolutionalConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 12, 8, 8)
//                .addConnectionAndLayer(
//                        new PoolingConn(), 12, 4, 4)
//                .addConnectionAndLayer(
//                        new MapToFullyConn())
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10); // 98.37

//        network.addFirstLayer(new MapLayer(1, 28, 28))
//                .addConnectionAndLayer(
//                        new ConvopoolingConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 20, 14, 14)
//                .addConnectionAndLayer(
//                        new ConvopoolingConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 15, 7, 7)
//                .addConnectionAndLayer(
//                        new MapToFullyConn())
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 100)
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10); //98.27

//        network.addFirstLayer(new MapLayer(1, 28, 28))
//                .addConnectionAndLayer(
//                        new ConvopoolingConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10, 7, 7)
//                .addConnectionAndLayer(
//                        new MapToFullyConn())
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10)
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 16)
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10); //96.08 T

//        network.addFirstLayer(new MapLayer(1, 28, 28))
//                .addConnectionAndLayer(
//                        new ConvopoolingConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 22, 7, 7)
//                .addConnectionAndLayer(
//                        new MapToFullyConn())
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 30)
//                .addConnectionAndLayer(
//                        new FullyConnectedConn(ActivationFunctions.SIGMOID, readerW,
//                        readerT), 10); //97.75 T 43 epochs

        NetReader mnistReader = new MnistReader(MINI_BATCH_SIZE,
                                                NUMBER_OF_FEATURES,
                                                NUMBER_OF_LEARNING_PICTURES,
                                                NUMBER_OF_VALID_PICTURES,
                                                NUMBER_OF_TESTING_PICTURES);
        train(network, mnistReader);
    }
}
