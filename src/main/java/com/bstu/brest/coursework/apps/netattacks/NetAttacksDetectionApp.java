package com.bstu.brest.coursework.apps.netattacks;

import com.bstu.brest.coursework.apps.AbstractApp;
import com.bstu.brest.coursework.architecture.connection.FullyConnectedConn;
import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.functions.CostFunctions;
import com.bstu.brest.coursework.architecture.layer.PerceptronLayer;
import com.bstu.brest.coursework.architecture.network.NetworkImpl;
import com.bstu.brest.coursework.apps.netreader.NetAttacksReader;
import com.bstu.brest.coursework.apps.netreader.NetReader;
import com.bstu.brest.coursework.architecture.reader.NormalReader;

import java.io.IOException;

public class NetAttacksDetectionApp extends AbstractApp {

    private static final int NUMBER_OF_ALL_SAMPLES = 494020;

    // 60000
    private static final int NUMBER_OF_LEARNING_SAMPLES = 352000;
    private static final int NUMBER_OF_VALID_SAMPLES = 71000;

    // 10000
    private static final int NUMBER_OF_TESTING_SAMPLES = 71000;

    private static final int NUMBER_OF_EPOCHS = 500; //1000

    // !!! 1, 10, 25
    private static final int MINI_BATCH_SIZE = 25;

    private static final int NUMBER_OF_FEATURES = 23;

    private static final double ALPHA = 0.1; //0.00001

    private static final CostFunctions COST_FUNCTION = CostFunctions.CROSS_ENTROPY;

    private static final boolean IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS = false;

    private static final boolean IS_DISPLAY_PN_MATRIX = false;

    private static final String W_FILE_NAME = "w4.txt";
    private static final String T_FILE_NAME = "t4.txt";

    public NetAttacksDetectionApp() {
        super(NUMBER_OF_LEARNING_SAMPLES, NUMBER_OF_VALID_SAMPLES,
              NUMBER_OF_TESTING_SAMPLES, NUMBER_OF_EPOCHS, false,
              false, IS_TO_SAVE_WEIGHTS_AND_THRESHOLDS,
              IS_DISPLAY_PN_MATRIX, null, null,
              W_FILE_NAME, T_FILE_NAME);
    }

    @Override
    public void main(String[] args) throws IOException {
        NormalReader readerW = new NormalReader(true);
        NormalReader readerT = new NormalReader();

        NetworkImpl network = new NetworkImpl(this, COST_FUNCTION, MINI_BATCH_SIZE, ALPHA,
                                              readerW, readerT);

        network.addFirstLayer(new PerceptronLayer(MINI_BATCH_SIZE, 120))
                .addConnectionAndLayer(
                        new FullyConnectedConn(ActivationFunctions.SIGMOID),
                        MINI_BATCH_SIZE, 40)
                .addConnectionAndLayer(
                        new FullyConnectedConn(ActivationFunctions.SIGMOID),
                        MINI_BATCH_SIZE, NUMBER_OF_FEATURES); //99.26 V // 99.28 T

        NetReader netAttacksReader = new NetAttacksReader(MINI_BATCH_SIZE,
                                                          NUMBER_OF_FEATURES,
                                                          NUMBER_OF_LEARNING_SAMPLES,
                                                          NUMBER_OF_VALID_SAMPLES,
                                                          NUMBER_OF_TESTING_SAMPLES);
        train(network, netAttacksReader);
    }
}
