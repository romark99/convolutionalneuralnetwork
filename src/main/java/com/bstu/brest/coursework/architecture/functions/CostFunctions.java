package com.bstu.brest.coursework.architecture.functions;

public enum CostFunctions {
    QUADRATIC("QUADRATIC", HelpFunctions::quadraticFunction,
              HelpFunctions::deltaQuadraticFunction),
    CROSS_ENTROPY("CROSS_ENTROPY", HelpFunctions::crossEntropyFucntion,
                  HelpFunctions::deltaCrossEntropyFucntion);

    private final String value;
    private final CostFunction costFunction;
    private final DeltaCostFunction deltaFunction;

    CostFunctions(String value, CostFunction costFunction,
                  DeltaCostFunction deltaFunction) {
        this.value = value;
        this.costFunction = costFunction;
        this.deltaFunction = deltaFunction;
    }

    public String getValue() {
        return value;
    }

    public CostFunction getCostFunction() {
        return costFunction;
    }

    public DeltaCostFunction getDeltaFunction() {
        return deltaFunction;
    }
}
