package com.bstu.brest.coursework.architecture.layer;

import com.bstu.brest.coursework.types.DoubleSet;

public interface Filter extends DoubleSet {

    double getThreshold();

    void setThreshold(double threshold);

    void appendToThreshold(double value);

    int getWidth();

    int getHeight();
}
