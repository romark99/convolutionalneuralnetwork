package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.layer.LayerImpl;
import com.bstu.brest.coursework.architecture.reader.Readable;

/**
 * Connection class with weights.
 *
 * @param <L> - type of left layer.
 * @param <R> - type of right layer.
 */
public abstract class ConnectionChangeWeightsImpl<L extends LayerImpl,
        R extends LayerImpl>
        extends ConnectionImpl<L, R>
        implements ChangeWeightable {

    /**
     * Learning step.
     */
    protected final double alpha;

    /**
     * Learning step.
     */
    protected static double ALPHA;

    /**
     * Activation and derivative functions.
     */
    protected final ActivationFunctions functions;

    /**
     * Way of reading weights and thresholds.
     */
    protected static Readable readerW, readerT;

    public static void setReaderW(Readable readerW) {
        ConnectionChangeWeightsImpl.readerW = readerW;
    }

    public static void setReaderT(Readable readerT) {
        ConnectionChangeWeightsImpl.readerT = readerT;
    }

    /**
     * Constructor. THIS VERSION DOES NOT INITIALIZE WEIGHTS and THRESHOLDS.
     */
    ConnectionChangeWeightsImpl(L leftLayer, R rightLayer, double alpha,
                                ActivationFunctions functions) {
        super(leftLayer, rightLayer);
        if (functions == null || readerW == null || readerT == null) {
            throw new NullPointerException(
                    "ConnectionChangeWeightsImpl: All arguments are not null.");
        }
        if (alpha <= 0. || alpha > 1.) {
            throw new IllegalArgumentException(
                    "ConnectionChangeWeightsImpl: Alpha should be in interval (0; 1].");
        }
        this.alpha = alpha;
        this.functions = functions;
    }

    /**
     * Constructor. THIS VERSION DOES NOT INITIALIZE WEIGHTS and THRESHOLDS.
     */
    ConnectionChangeWeightsImpl(double alpha, ActivationFunctions functions) {

        super();
        if (functions == null || readerW == null || readerT == null) {
            throw new NullPointerException(
                    "ConnectionChangeWeightsImpl: All arguments are not null.");
        }
        if (alpha <= 0.) {
            throw new IllegalArgumentException(
                    "ConnectionChangeWeightsImpl: Alpha should be more than 0.");
        }
        this.alpha = alpha;
        this.functions = functions;
    }

    public ConnectionChangeWeightsImpl(L leftLayer, R rightLayer, ActivationFunctions functions) {
        this(leftLayer, rightLayer, ALPHA, functions);
    }


    public ConnectionChangeWeightsImpl(ActivationFunctions functions) {
        this(ALPHA, functions);
    }

    public static void setDefaultAlpha(double alpha) {
        ConnectionChangeWeightsImpl.ALPHA = alpha;
    }

    /**
     * Changes weights and thresholds.
     */
    public abstract void changeWeightsAndThresholds();


    public double getAlpha() {
        return alpha;
    }

    public ActivationFunctions getFunctions() {
        return functions;
    }
}
