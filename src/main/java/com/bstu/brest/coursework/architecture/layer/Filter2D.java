package com.bstu.brest.coursework.architecture.layer;

import com.bstu.brest.coursework.types.DoubleSetImpl;

/**
 * Filter in convolutional network.
 */
public class Filter2D extends DoubleSetImpl implements Filter  {

    /**
     * The threshold of filter.
     */
    private double threshold;

    /**
     * Sizes of the filter.
     */
    private final int width, height;

    /**
     * Constructor.
     */
    public Filter2D(int... args) {
        super(args);
        this.width = size[0];
        this.height = size[1];
    }

    @Override
    public double getThreshold() {
        return threshold;
    }

    @Override
    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    @Override
    public void appendToThreshold(double value) {
        this.threshold += value;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }
}
