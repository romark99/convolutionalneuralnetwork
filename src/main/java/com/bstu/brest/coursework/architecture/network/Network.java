package com.bstu.brest.coursework.architecture.network;

import com.bstu.brest.coursework.types.BatchData;
import com.bstu.brest.coursework.types.ShortSet;

import java.util.ArrayList;

public interface Network {

    /**
     * NetworkImpl learning.
     * epochNumber - number of current epoch
     *
     * @return current error.
     */
    double learn(ArrayList<BatchData> trainingData, Integer epochNumber);

    /**
     * Gets predicted labels in batch.
     *
     * @return result.
     */
    ShortSet getResult();

    int validPredict(ArrayList<BatchData> validData, String wrongResultsPath, boolean displayPNMatrix);

    int testPredict(ArrayList<BatchData> testData, String wrongResultsPath, boolean displayPNMatrix);

    /**
     * Stores weights and thresholds in files.
     */
    void saveWeightsAndThresholds(String weightsPath, String thresholdsPath);

    void displayFinalResults(long trainingDuration, int maxValidResult, int maxTestResult, int bestEpochIndex);
}
