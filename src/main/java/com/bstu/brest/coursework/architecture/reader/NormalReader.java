package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.types.DoubleSet;

import java.util.Random;

public class NormalReader implements Readable {

    private boolean isSmallInit;

    /**
     * Constructor.
     */
    public NormalReader(boolean isSmallInit) {
        this.isSmallInit = isSmallInit;
    }

    /**
     * Constructor.
     */
    public NormalReader() {
        this.isSmallInit = false;
    }

    @Override
    public void read(DoubleSet doubleSet) {
        Random rand = new Random();
//        for (int i = 0; i < doubleSet.x.length; i++) {
//            double val = rand.nextGaussian();
//            doubleSet.x[i] = val;
//        }
        for (int i = 0; i < doubleSet.getCount(); i++) {
            double val = rand.nextGaussian();
            if (isSmallInit) {
                val /= Math.sqrt(doubleSet.getSize()[1]);
            }
            doubleSet.setBySingleIndex(val, i);
        }
    }
}
