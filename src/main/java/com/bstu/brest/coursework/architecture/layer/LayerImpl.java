package com.bstu.brest.coursework.architecture.layer;

import com.bstu.brest.coursework.types.DoubleSetImpl;

/**
 * Represent the implementation of Layer interface. Contains neurons.
 */
public abstract class LayerImpl extends DoubleSetImpl implements Layer {

    /**
     * LayerImpl constructor.
     *
     * @param args - size of array with neurons.
     */
    LayerImpl(int... args) {
        super(args);
    }

    @Override
    public double get(int... indexes) {
        return super.get(indexes);
    }

    @Override
    public void set(double value, int... indexes) {
        super.set(value, indexes);
    }

    @Override
    public void append(double value, int... indexes) {
        super.append(value, indexes);
    }
}
