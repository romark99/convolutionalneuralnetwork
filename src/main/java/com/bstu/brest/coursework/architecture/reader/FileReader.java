package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.types.DoubleSet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

/**
 * Reads data from a file.
 */
public class FileReader implements Readable {

    /**
     * Reader.
     */
    private BufferedReader br;

    /**
     * Constructor which creates new FileStream and BufferedReader.
     *
     * @param fileName - file's name.
     */
    public FileReader(String fileName) throws FileNotFoundException {
        FileInputStream fstream = new FileInputStream(fileName);
        this.br = new BufferedReader(new InputStreamReader(fstream));
    }

    @Override
    public void read(DoubleSet doubleSet) {
        if (doubleSet == null) {
            throw new NullPointerException("FileReader: read: doubleSet is null.");
        }
        for (int i = 0; i < doubleSet.getCount(); i++) {
            try {
                String strLine = br.readLine();
                doubleSet.setBySingleIndex(Double.parseDouble(strLine), i);
            } catch (Exception e) {
                e.printStackTrace();
                throw new IllegalArgumentException("FileReader: read: file is ended.");
            }
        }
    }
}
