package com.bstu.brest.coursework.architecture.functions;

public enum ActivationFunctions {
    LINEAR("LINEAR", HelpFunctions::linearFunction,
           HelpFunctions::derivativeLinearFunction),
    SIGMOID("SIGMOID", HelpFunctions::sigmoidFunction,
            HelpFunctions::derivativeSigmoidFunction),
    RELU("RELU", HelpFunctions::reluFunction, HelpFunctions::derivativeReluFunction),
    TANH("TANH", HelpFunctions::tanhFunction, HelpFunctions::derivativeTanhFunction);

    private final String value;

    private final ActivationFunction activationFunction;

    private final ActivationFunction derivativeFunction;

    ActivationFunctions(String value, ActivationFunction activationFunction,
                        ActivationFunction derivativeFunction) {
        this.value = value;
        this.activationFunction = activationFunction;
        this.derivativeFunction = derivativeFunction;
    }

    public String getValue() {
        return value;
    }

    public ActivationFunction getActivationFunction() {
        return activationFunction;
    }

    public ActivationFunction getDerivativeFunction() {
        return derivativeFunction;
    }
}
