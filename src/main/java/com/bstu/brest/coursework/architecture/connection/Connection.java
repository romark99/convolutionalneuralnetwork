package com.bstu.brest.coursework.architecture.connection;

/**
 * Connection interface.
 */
public interface Connection {

    /**
     * Converts the values from the left layer to the right layer.
     */
    void activate();

}
