package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.layer.MapLayer;
import com.bstu.brest.coursework.architecture.layer.PerceptronLayer;

/**
 * Connection which turns MapLayer into PerceptronLayer.
 */
public class MapToFullyConn extends ConnectionImpl<MapLayer, PerceptronLayer>
        implements Backpropagational {

    /**
     * Constructor with layers.
     */
    MapToFullyConn(MapLayer leftLayer, PerceptronLayer rightLayer) {
        super(leftLayer, rightLayer);
        if (leftLayer == null || rightLayer == null) {
            throw new NullPointerException(
                    "MapToFullyConn(): leftLayer or rightLayer is null.");
        }
        if (leftLayer.getCount() != rightLayer.getCount()) {
            throw new IllegalArgumentException(
                    "MapToFullyConn(): leftLayer and rightLayer have different number " +
                    "of neurons.");
        }
    }

    /**
     * Constructor without params.
     */
    public MapToFullyConn() {
        super();
    }

    @Override
    public void activate() {
        if (rightLayer.getCount() > 0) {
            for (int i = 0; i < rightLayer.getCount(); i++) {
                rightLayer.setBySingleIndex(leftLayer.getBySingleIndex(i), i);
            }
        }
    }

    @Override
    public PerceptronLayer generateLayer(int... size) {
        if (this.rightLayer != null) {
            throw new UnsupportedOperationException(
                    "MapToFullyConn: generateLayer(): right layer in connection are " +
                    "already not null.");
        }
        if (this.leftLayer == null) {
            throw new UnsupportedOperationException(
                    "MapToFullyConn: generateLayer(): left layer should not be null. " +
                    "You probably forgot to call 'addFirstLayr()");
        }
        if (size.length != 2) {
            throw new IllegalArgumentException(
                    "MapToFullyConn: generateLayer(): two params are required.");
        }
        if (size[0] * size[1] != leftLayer.getCount()) {
            throw new IllegalArgumentException(
                    "MapToFullyConn: generateLayer(): wrong size of right layer.");
        }
        PerceptronLayer rightLayer = new PerceptronLayer(size);
        this.rightLayer = rightLayer;
        return rightLayer;
    }

    @Override
    public void backpropagation() {
        for (int i = 0; i < leftLayer.getGamma().getCount(); i++) {
            leftLayer.getGamma().setBySingleIndex(rightLayer.getGamma().getBySingleIndex(i), i);
        }
    }
}
