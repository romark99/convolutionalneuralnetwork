package com.bstu.brest.coursework.architecture.connection;

/**
 * Entity which can change weights and thresholds.
 */
public interface ChangeWeightable {

    /**
     * Changes weights and thresholds.
     */
    void changeWeightsAndThresholds();
}
