package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.architecture.functions.ActivationFunction;
import com.bstu.brest.coursework.types.DoubleSet;

/**
 * Reads data from function.
 */
public class FunctionReader implements Readable {

    /**
     * Function.
     */
    public ActivationFunction function;

    /**
     * Size of doubleSet.
     */
    public int size;

    /**
     * Function's interval.
     */
    double start, end;

    /**
     * Distance from x to x+1.
     */
    double step;

    /**
     * x-s.
     */
    public double[] x;

    /**
     * Constructor.
     * size - size of array.
     * start, end - interval.
     *
     * @param function - function.
     */
    public FunctionReader(ActivationFunction function, int size, double start,
                          double end) {
        if (function == null) {
            throw new NullPointerException("FunctionReader(): function is null.");
        }
        if (size <= 0) {
            throw new IllegalArgumentException(
                    "FunctionReader(): size should be positive.");
        }
        if (start >= end) {
            throw new IllegalArgumentException(
                    "FunctionReader(): start should be less than end.");
        }
        this.function = function;
        this.size = size;
        this.start = start;
        this.end = end;
        if (size != 1) {
            this.step = (end - start) / (size - 1);
        }
        this.x = new double[size];
        for (int i = 0; i < size; i++) {
            x[i] = i * step + start;
        }
    }

    @Override
    public void read(DoubleSet doubleSet) {
        if (doubleSet.getNumOfDimensions() != 1) {
            throw new IllegalArgumentException(
                    "FunctionReader: read(): wrong doubleSet numberOfDimensions");
        }
        if (doubleSet.getSize()[0] != size) {
            throw new IllegalArgumentException(
                    "FunctionReader: read(): size of doubleSet and constructor size are" +
                    " not equal.");
        }
        for (int i = 0; i < x.length; i++) {
            doubleSet.setBySingleIndex(function.function(x[i]), i);
        }
    }
}
