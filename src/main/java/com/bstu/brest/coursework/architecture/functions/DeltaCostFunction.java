package com.bstu.brest.coursework.architecture.functions;

public interface DeltaCostFunction {
    Double function(Double output, Double etalon, ActivationFunction derivativeFunction);
}