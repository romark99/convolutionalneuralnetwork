package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.architecture.functions.HelpFunctions;
import com.bstu.brest.coursework.types.DoubleSet;

/**
 * Reads random data.
 */
public class RandomReader implements Readable {

    /**
     * Left border of data.
     */
    private double leftBorder;

    /**
     * Right border of data.
     */
    private double rightBorder;

    /**
     * Constructor.
     */
    public RandomReader(double leftBorder, double rightBorder) {
        if (leftBorder > rightBorder) {
            throw new IllegalArgumentException(
                    "RandomReader(): leftBorder should not be more than rightBorder.");
        }
        this.leftBorder = leftBorder;
        this.rightBorder = rightBorder;
    }

    @Override
    public void read(DoubleSet doubleSet) {
        double sum = 0.;
        for (int i = 0; i < doubleSet.getCount(); i++) {
            double val = HelpFunctions.random(leftBorder, rightBorder);
            doubleSet.setBySingleIndex(val, i);
            sum += val;
        }
        if (sum == 0.) {
            return;
        }
        for (int i = 0; i < doubleSet.getCount(); i++) {
            double val = doubleSet.getBySingleIndex(i);
            val /= (sum * 10);
            doubleSet.setBySingleIndex(val, i);
        }
    }
}
