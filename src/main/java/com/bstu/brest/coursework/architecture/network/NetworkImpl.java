package com.bstu.brest.coursework.architecture.network;

import com.bstu.brest.coursework.apps.App;
import com.bstu.brest.coursework.architecture.connection.*;
import com.bstu.brest.coursework.architecture.functions.ActivationFunction;
import com.bstu.brest.coursework.architecture.functions.CostFunctions;
import com.bstu.brest.coursework.architecture.layer.Layer;
import com.bstu.brest.coursework.architecture.layer.LayerImpl;
import com.bstu.brest.coursework.architecture.layer.LayerWithGammas;
import com.bstu.brest.coursework.architecture.reader.Readable;
import com.bstu.brest.coursework.types.BatchData;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.ImageData;
import com.bstu.brest.coursework.types.ShortSet;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Any network class.
 */
public class NetworkImpl implements Network {

    private final CostFunctions costFunctions;

    /**
     * Current error of the network.
     */
    private double E;

    /**
     * Layers.
     */
    private final ArrayList<LayerImpl> layers;

    private final int MINI_BATCH_SIZE;

    private final int LEARNING_PICTURES_LENGTH;

    private final int TESTING_PICTURES_LENGTH;

    private final int VALID_PICTURES_LENGTH;

    /**
     * Connections.
     */
    private final ArrayList<ConnectionImpl> connections;

    public NetworkImpl(App app, CostFunctions costFunctions, int miniBatchSize,
                       double defaultAlpha,
                       Readable readerW,
                       Readable readerT) {
        this.costFunctions = costFunctions;
        this.layers = new ArrayList<>();
        this.connections = new ArrayList<>();
        this.LEARNING_PICTURES_LENGTH = app.getNumberOfLearningSamples();
        this.TESTING_PICTURES_LENGTH = app.getNumberOfTestingSamples();
        this.VALID_PICTURES_LENGTH = app.getNumberOfValidSamples();
        this.MINI_BATCH_SIZE = miniBatchSize;

        ConnectionChangeWeightsImpl.setDefaultAlpha(defaultAlpha);
        ConnectionChangeWeightsImpl.setReaderW(readerW);
        ConnectionChangeWeightsImpl.setReaderT(readerT);
    }

    /**
     * Adds first layer to the network.
     */
    public NetworkImpl addFirstLayer(LayerImpl layer) {
        if (layer == null) {
            throw new NullPointerException(
                    "NetworkImpl: addFirstLayer(): layer is null.");
        }
        if (!layers.isEmpty()) {
            throw new UnsupportedOperationException(
                    "NetworkImpl: addFirstLayer(): first layer already exists.");
        }
        layers.add(layer);
        return this;
    }

    private void addConnection(ConnectionImpl connection) {
        connections.add(connection);
    }

    /**
     * ONLY ADDS A CONNECTION
     * layer is added in extended functions.
     * Adds connection with generated layer to the network.
     *
     * @param connection - connection.
     * @param size       - size of right layer.
     * @return edited network.
     */
    public NetworkImpl addConnectionAndLayer(ConnectionImpl connection, int... size)
            throws IOException {
        if (this.layers.isEmpty()) {
            throw new UnsupportedOperationException(
                    "NetworkImpl: addConnectionAndLayer(): there is no first layer in " +
                    "the network. You should first call 'addFirstLayer' function.");
        }
        for (int sz : size) {
            if (sz <= 0) {
                throw new IllegalArgumentException(
                        "NetworkImpl: addConnectionAndLayer(): layer dimensions should " +
                        "be positive numbers.");
            }
        }
        if (connection == null) {
            throw new NullPointerException(
                    "NetworkImpl: addConnectionAndLayer(): connection is null.");
        }
        addConnection(connection);
        connection.setLeftLayer(this.layers.get(this.layers.size() - 1));
        LayerWithGammas rightLayer;
        if (connection instanceof MapToFullyConn) {
            if (size.length != 0) {
                throw new IllegalArgumentException(
                        "ConvolutionalNetwork: addConnectionAndLayer(): for " +
                        "MapToFullyLayer: no params required for constructor.");
            }
            int length = connection.getLeftLayer().getCount();
            rightLayer = (LayerWithGammas) connection
                    .generateLayer(MINI_BATCH_SIZE, length / MINI_BATCH_SIZE);
        } else {
            rightLayer = (LayerWithGammas) connection.generateLayer(size);
        }
        this.layers.add(rightLayer);
        return this;
    }

    @Override
    public double learn(ArrayList<BatchData> trainingData, Integer epochNumber) throws ArrayIndexOutOfBoundsException {
        long startTime = System.currentTimeMillis();
        E = 0.;
        for (BatchData batchDatum : trainingData) {
            for (int j = 0; j < layers.get(0).getCount(); j++) {
                layers.get(0)
                        .setBySingleIndex(batchDatum.getImages()
                                                  .getBySingleIndex(j),
                                          j);
            }
            DoubleSet etalon = batchDatum.getLabels();
            for (ConnectionImpl connection : connections) {
                connection.activate();
            }
            Layer lastLayer = this.layers.get(layers.size() - 1);
            if (lastLayer instanceof LayerWithGammas) {
                E += this.backPropagationForLast(etalon,
                                                 (LayerWithGammas) lastLayer);
            }
            // TODO: backpropagation is unneccessary for the first layer.
            for (int j = connections.size() - 1; j >= 0; j--) {
                ConnectionImpl connImpl = connections.get(j);
                if (connImpl instanceof Backpropagational) {
                    Backpropagational conn = (Backpropagational) connImpl;
                    conn.backpropagation();
                }
            }
            for (int j = connections.size() - 1; j >= 0; j--) {
                ConnectionImpl connImpl = connections.get(j);
                if (connImpl instanceof ChangeWeightable) {
                    ChangeWeightable conn = (ChangeWeightable) connImpl;
                    conn.changeWeightsAndThresholds();
                }
            }
        }
        E /= LEARNING_PICTURES_LENGTH;
        long endTime = System.currentTimeMillis();
        System.out.println(
                "Epoch #" + epochNumber + " duration: " + (endTime - startTime) + "ms.");
        System.out.println("Error: " + E);
        return E;
    }

    @Override
    public int validPredict(ArrayList<BatchData> validData, String wrongResultsPath, boolean displayPNMatrix) {
        return executePrediction(validData, wrongResultsPath, displayPNMatrix);
    }

    @Override
    public int testPredict(ArrayList<BatchData> testingData, String wrongResultsPath, boolean displayPNMatrix) {
        return executePrediction(testingData, wrongResultsPath, displayPNMatrix);
    }

    private int executePrediction(ArrayList<BatchData> testingData, String path, boolean displayPNMatrix) {
        final int lastLayerCount = layers.get(layers.size() - 1).getSize()[1];
        int[][] positiveNegativeMatrix = new int[lastLayerCount][lastLayerCount];
        Map<ImageData, DoubleSet> wrongTestImages = predict(testingData,
                                                            positiveNegativeMatrix);
        if (path != null) {
            writeWrongResultsToFile(wrongTestImages, path);
        }
        if (displayPNMatrix) {
            displayPNMatrix(positiveNegativeMatrix);
        }
        return testingData.size() * testingData.get(0).getImages().getSize()[0] - wrongTestImages.size();
    }

    private void displayPNMatrix(int[][] positiveNegativeMatrix) {
        System.out.println("PN Matrix: ");
        System.out.print("      ");
        int n = positiveNegativeMatrix.length;
        for (int i = 0; i < n; i++) {
            System.out.printf("%6d", i);
        }
        System.out.print("\n      ");
        for (int i = 0; i < n; i++) {
            System.out.print("      ");
        }
        for (int i = 0; i < n; i++) {
            System.out.print("\n      ");
            for (int j = 0; j < n; j++) {
                System.out.print("      ");
            }
            System.out.printf("\n%5d ", i);
            for (int j = 0; j < n; j++) {
                System.out.printf("%6d", positiveNegativeMatrix[i][j]);
            }
        }
        System.out.println();
        System.out.println();
    }

    public void displayFinalResults(long duration, int maxValidResult, int maxTestResult,
                                    int bestEpochIndex) {
        System.out.println("---------------------------");
        System.out.println("Error: " + E);
        System.out.println("Total time: " + duration + "ms.");
        System.out.printf("Best epoch: #%d\n", bestEpochIndex);
        System.out.printf("Best valid result: %d / %d\n", maxValidResult,
                          VALID_PICTURES_LENGTH);
        System.out.printf("Best test result: %d / %d\n", maxTestResult,
                          TESTING_PICTURES_LENGTH);
        System.out.print("The end.");
    }


    private Map<ImageData, DoubleSet> predict(ArrayList<BatchData> testData, int[][] positiveNegativeMatrix) {
        Map<ImageData, DoubleSet> wrongImages = new HashMap<>();
        for (BatchData testBatchDatum : testData) {
            for (int i = 0; i < layers.get(0).getCount(); i++) {
                layers.get(0)
                        .setBySingleIndex(
                                testBatchDatum.getImages().getBySingleIndex(i),
                                i);
            }
            for (ConnectionImpl connection : connections) {
                connection.activate();
            }
            ShortSet predictedLabels = getResult();
            for (int i = 0; i < MINI_BATCH_SIZE; i++) {
                if (predictedLabels.get(i) != testBatchDatum.getLabelNumbers().get(i)) {
                    ImageData imageDatum = testBatchDatum.getImageDatumByIndex(i);
                    DoubleSet copiedLastLayer = this.layers.get(this.layers.size() - 1)
                            .getSmallerDoubleSetByIndex(i);
                    wrongImages.put(imageDatum, copiedLastLayer);
                }
                positiveNegativeMatrix[predictedLabels.get(i)][testBatchDatum.getLabelNumbers().get(i)]++;
            }
        }
        return wrongImages;
    }

    private void writeWrongResultsToFile(Map<ImageData, DoubleSet> wrongImages,
                                         String path) {
        try (BufferedWriter wResults = new BufferedWriter(
                new FileWriter(path, false))) {
            wrongImages.forEach((wrongImage, lastLayer) -> {
                try {
                    wrongImage.drawImage(wResults, lastLayer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ShortSet getResult() {
        if (this.layers != null && this.layers.size() > 0) {
            ShortSet labelNumbers = new ShortSet(MINI_BATCH_SIZE);
            LayerImpl lastLayer = this.layers.get(this.layers.size() - 1);
            for (int j = 0; j < MINI_BATCH_SIZE; j++) {
                if (lastLayer.getNumOfDimensions() == 2) {
                    short label = 0;
                    double max = lastLayer.get(j, 0);
                    for (short i = 1; i < lastLayer.getSize()[1]; i++) {
                        if (max < lastLayer.get(j, i)) {
                            max = lastLayer.get(j, i);
                            label = i;
                        }
                    }
                    labelNumbers.set(label, j);
                } else {
                    labelNumbers.set((short) -1, j);
                }
            }
            return labelNumbers;
        }
        return null;
    }

    private double backPropagationForLast(DoubleSet etalon, LayerWithGammas lastLayer) {
        double sum = 0.;
        for (int u = 0; u < MINI_BATCH_SIZE; u++) {
            ConnectionChangeWeightsImpl lastConnection = null;
            ActivationFunction derivativeFunction = null;
            for (int i = connections.size() - 1; i >= 0; i--) {
                if (connections.get(i) instanceof ConnectionChangeWeightsImpl) {
                    lastConnection = (ConnectionChangeWeightsImpl) connections
                            .get(connections.size() - 1);
                    derivativeFunction = lastConnection.getFunctions()
                            .getDerivativeFunction();
                    break;
                }
            }
            for (int i = 0; i < etalon.getSize()[1]; i++) {
                assert lastConnection != null;
                lastLayer.getGamma().set(costFunctions.getDeltaFunction()
                                                 .function(lastLayer.get(u, i),
                                                           etalon.get(u, i),
                                                           derivativeFunction), u, i);
                sum += costFunctions.getCostFunction()
                        .function(lastLayer.get(u, i), etalon.get(u, i));
            }
        }
        return sum;
    }

    @Override
    public void saveWeightsAndThresholds(String weightsPath, String thresholdsPath) {
        try (BufferedWriter wBw = new BufferedWriter(
                new FileWriter(weightsPath, false));
             BufferedWriter tBw = new BufferedWriter(
                     new FileWriter(thresholdsPath, false))) {
            for (ConnectionImpl connection : connections) {
                if (connection instanceof FullyConnectedConn) {
                    FullyConnectedConn fullyConnectedConn =
                            (FullyConnectedConn) connection;
                    for (int i = 0; i < fullyConnectedConn.getWeights().getCount(); i++) {
                        wBw.write(String.valueOf(
                                fullyConnectedConn.getWeights().getBySingleIndex(i)));
                        wBw.append('\n');
                    }
                    for (int i = 0;
                         i < fullyConnectedConn.getRightLayer().getThresholds()
                                 .getCount(); i++) {
                        tBw.write(String.valueOf(
                                fullyConnectedConn.getRightLayer().getThresholds()
                                        .getBySingleIndex(i)));
                        tBw.append('\n');
                    }
                } else if (connection instanceof ConvolutionalConn3D) {
                    ConvolutionalConn3D convolutionalConn = (ConvolutionalConn3D) connection;
                    for (int i = 0; i < convolutionalConn.getFilters().length; i++) {
                        for (int t = 0;
                             t < convolutionalConn.getFilters()[i].getCount(); t++) {
                            wBw.write(String.valueOf(
                                    convolutionalConn.getFilters()[i]
                                            .getBySingleIndex(t)));
                            wBw.append('\n');
                        }
                        tBw.write(String.valueOf(
                                convolutionalConn.getFilters()[i].getThreshold()));
                        tBw.append('\n');
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
