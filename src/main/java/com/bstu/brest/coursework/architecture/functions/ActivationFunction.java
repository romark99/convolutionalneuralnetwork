package com.bstu.brest.coursework.architecture.functions;

/**
 * Represents function.
 */
public interface ActivationFunction {

    /**
     * Function.
     *
     * @param x - argument.
     * @return - returned value.
     */
    Double function(Double x);
}
