package com.bstu.brest.coursework.architecture.layer;

/**
 * Layer interface.
 */
public interface Layer {

    /**
     * Gets neuron of layer.
     *
     * @param indexes - indexes.
     * @return neuron.
     */
    double get(int... indexes);

    /**
     * Sets neuron of layer.
     *
     * @param value   - new value.
     * @param indexes - indexes.
     */
    void set(double value, int... indexes);

    /**
     * Appends the value to the neuron.
     *
     * @param value   - value.
     * @param indexes - indexes.
     */
    void append(double value, int... indexes);

}
