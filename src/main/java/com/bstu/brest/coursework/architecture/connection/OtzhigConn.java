package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.layer.LayerImpl;
import com.bstu.brest.coursework.architecture.reader.Readable;

public class OtzhigConn extends ConnectionChangeWeightsImpl<LayerImpl, LayerImpl> {
    public OtzhigConn(LayerImpl leftLayer, LayerImpl rightLayer,
                      ActivationFunctions functions) {
        super(leftLayer, rightLayer, functions);
    }

    @Override
    public void changeWeightsAndThresholds() {

    }

    @Override
    public void activate() {

    }

    @Override
    public LayerImpl generateLayer(int... size) {
        return null;
    }

}
