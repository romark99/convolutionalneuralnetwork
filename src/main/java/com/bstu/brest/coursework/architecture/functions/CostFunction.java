package com.bstu.brest.coursework.architecture.functions;

public interface CostFunction {
    Double function(Double output, Double etalon);
}