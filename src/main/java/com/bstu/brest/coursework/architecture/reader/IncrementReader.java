package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.types.DoubleSet;

public class IncrementReader implements Readable {

    private double currentValue;

    private double step;

    public IncrementReader(double startValue, double step) {
        this.currentValue = startValue;
        this.step = step;
    }

    @Override
    public void read(DoubleSet doubleSet) {
        if (doubleSet == null) {
            throw new NullPointerException("IncrementReader: read: doubleSet is null.");
        }
//        for (int i = 0; i < doubleSet.x.length; i++) {
//            doubleSet.x[i] = currentValue;
//            currentValue += step;
//        }
        for (int i = 0; i < doubleSet.getCount(); i++) {
            doubleSet.setBySingleIndex(currentValue, i);
            currentValue += step;
        }
    }

}