package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.types.DoubleSet;

/**
 * Reader from array.
 */
public class ArrayReader implements Readable {

    /**
     * Array.
     */
    double[] array;

    /**
     * Constructor.
     *
     * @param array - array.
     */
    public ArrayReader(double[] array) {
        if (array == null) {
            throw new NullPointerException("ArrayReader: array is not null.");
        }
        this.array = array;
    }

    @Override
    public void read(DoubleSet doubleSet) {
        if (doubleSet == null) {
            throw new NullPointerException("ArrayReader: read: doubleSet is null.");
        }
        if (doubleSet.getCount() != array.length) {
            throw new IllegalArgumentException(
                    "ArrayReader: read: doubleSet and array have different lengths.");
        }
        for (int i = 0; i < array.length; i++) {
            doubleSet.setBySingleIndex(array[i], i);
        }
    }
}
