package com.bstu.brest.coursework.architecture.layer;

/**
 * Map in convolutional network. NOT USED.
 */
public class Map extends LayerImpl {

    /**
     * Width of map.
     */
    private int width;

    /**
     * Height of map.
     */
    private int height;

    /**
     * Constructor for map.
     */
    Map(int... args) {
        super(args);
        if (args.length != 2) {
            throw new IllegalArgumentException("Map(): args should have 2 numbers.");
        }
        this.width = args[0];
        this.height = args[1];
    }
}
