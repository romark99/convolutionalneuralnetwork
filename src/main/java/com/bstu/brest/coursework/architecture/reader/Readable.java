package com.bstu.brest.coursework.architecture.reader;

import com.bstu.brest.coursework.types.DoubleSet;

import java.io.IOException;

/**
 * Allows to read weights and thresholds.
 */
public interface Readable {

    /**
     * Reads data to doubleSet.
     *
     * @param doubleSet - doubleSet.
     */
    void read(DoubleSet doubleSet) throws IOException;
}
