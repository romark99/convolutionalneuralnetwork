package com.bstu.brest.coursework.architecture.layer;

import com.bstu.brest.coursework.types.DoubleSet;

/**
 * One-dimensional array of neurons. Represents a layer of perceptron.
 */
public class PerceptronLayer extends LayerWithGammas {

    /**
     * Thresholds between two layers. They are stored in the connection's right layer.
     */
    private DoubleSet thresholds;

    /**
     * Length of layer.
     */
    private final int length;

    /**
     * Constructor. DOES NOT CREATE THRESHOLD ARRAY.
     *
     * @param size - two numbers: minibatch_size, size of perceptron layer.
     */
    public PerceptronLayer(int... size) {
        super(size);
        if (size.length != 2) {
            throw new IllegalArgumentException(
                    "PerceptronLayer constructor should have 2 params!");
        }
        length = size[1];
    }

    public DoubleSet getThresholds() {
        return thresholds;
    }

    public int getLength() {
        return length;
    }

    public void setThresholds(DoubleSet thresholds) {
        this.thresholds = thresholds;
    }
}
