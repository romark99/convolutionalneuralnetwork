package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.layer.LayerImpl;

import java.io.IOException;

/**
 * Implementation of Connection.
 *
 * @param <L> - type of left layer.
 * @param <R> - type of right layer.
 */
public abstract class ConnectionImpl<L extends LayerImpl, R extends LayerImpl>
        implements Connection {

    /**
     * Left layer.
     */
    protected L leftLayer;

    /**
     * Right layer.
     */
    protected R rightLayer;

    /**
     * Generates right layer AND WEIGHTS WITH THRESHOLDS IF NEEDED.
     *
     * @param size - size of right layer.
     * @return right layer.
     */
    public abstract R generateLayer(int... size) throws IOException;

    /**
     * Constructor.
     */
    ConnectionImpl() {
    }

    /**
     * Constructor.
     *
     * @param leftLayer  - left layer.
     * @param rightLayer - right layer.
     */
    ConnectionImpl(L leftLayer, R rightLayer) {
        this.leftLayer = leftLayer;
        this.rightLayer = rightLayer;
    }

    public void setLeftLayer(L leftLayer) {
        this.leftLayer = leftLayer;
    }

    public L getLeftLayer() {
        return leftLayer;
    }

    public R getRightLayer() {
        return rightLayer;
    }
}
