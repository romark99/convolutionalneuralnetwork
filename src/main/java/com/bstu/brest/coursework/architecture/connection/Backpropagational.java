package com.bstu.brest.coursework.architecture.connection;

/**
 * Entity which can backpropagate.
 */
public interface Backpropagational {

    /**
     * Counts gammas for the hidden layers.
     */
    void backpropagation();
}
