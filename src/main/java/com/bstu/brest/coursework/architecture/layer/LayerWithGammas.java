package com.bstu.brest.coursework.architecture.layer;

import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;

/**
 * Layer with gammas.
 */
public abstract class LayerWithGammas extends LayerImpl {

    /**
     * Errors between two layers. They are stored in the connection's right layer.
     */
    private final DoubleSet gamma;

    private final int miniBatchSize;

    /**
     * Constructor, creates gamma array.
     *
     * @param args - indexes.
     */
    LayerWithGammas(int... args) {
        super(args);
        miniBatchSize = args[0];
        gamma = new DoubleSetImpl(args);
    }

    public DoubleSet getGamma() {
        return gamma;
    }

    public int getMiniBatchSize() {
        return miniBatchSize;
    }
}
