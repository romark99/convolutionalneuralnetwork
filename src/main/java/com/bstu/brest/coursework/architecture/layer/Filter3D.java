package com.bstu.brest.coursework.architecture.layer;

import com.bstu.brest.coursework.types.DoubleSetImpl;

/**
 * Filter in convolutional network.
 */
public class Filter3D extends DoubleSetImpl implements Filter  {

    /**
     * The threshold of filter.
     */
    private double threshold;

    /**
     * Sizes of the filter.
     */
    private final int depth, width, height;

    /**
     * Constructor.
     */
    public Filter3D(int... args) {
        super(args);
        this.depth = size[0];
        this.width = size[1];
        this.height = size[2];
    }

    @Override
    public double getThreshold() {
        return threshold;
    }

    @Override
    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    @Override
    public void appendToThreshold(double value) {
        this.threshold += value;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }
}
