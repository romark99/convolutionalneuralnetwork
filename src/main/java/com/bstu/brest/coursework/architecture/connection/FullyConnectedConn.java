package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.layer.PerceptronLayer;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;

import java.io.IOException;

/**
 * Connection between perceptron layers.
 */
public class FullyConnectedConn
        extends ConnectionChangeWeightsImpl<PerceptronLayer, PerceptronLayer>
        implements Backpropagational {

    /**
     * Weights of the connection.
     */
    private DoubleSet weights;

    public DoubleSet getWeights() {
        return weights;
    }

    /**
     * Constructor. SHOULD INITIALIZE THRESHOLDS.
     */
    public FullyConnectedConn(PerceptronLayer leftLayer, PerceptronLayer rightLayer,
                              double alpha, ActivationFunctions functions) throws IOException {
        super(leftLayer, rightLayer, alpha, functions);
        if (leftLayer == null || rightLayer == null) {
            throw new NullPointerException(
                    "FullyConnectedConn(): one of the layer was null. Use another " +
                    "constructor");
        }
        if (leftLayer.getMiniBatchSize() != rightLayer.getMiniBatchSize()) {
            throw new IllegalArgumentException(
                    "FullyConnectedConn(): miniBatchSizes are not equal.");
        }
        int miniBatchSize = leftLayer.getMiniBatchSize();
        weights = new DoubleSetImpl(miniBatchSize, leftLayer.getLength(), rightLayer.getLength());
        readerW.read(weights);
        rightLayer.setThresholds(new DoubleSetImpl(miniBatchSize, rightLayer.getLength()));
        readerT.read(rightLayer.getThresholds());
    }

    /**
     * Constructor. SHOULD INITIALIZE THRESHOLDS.
     */
    public FullyConnectedConn(double alpha, ActivationFunctions functions) {
        super(alpha, functions);
    }

    public FullyConnectedConn(PerceptronLayer leftLayer, PerceptronLayer rightLayer,
                              ActivationFunctions functions) throws IOException {
        this(leftLayer, rightLayer, ALPHA, functions);
    }

    /**
     * Constructor. SHOULD INITIALIZE THRESHOLDS.
     */
    public FullyConnectedConn(ActivationFunctions functions) {
        this(ALPHA, functions);
    }

    @Override
    public void changeWeightsAndThresholds() {
        for (int u = 0; u < leftLayer.getMiniBatchSize(); u++) {
            for (int i = 0; i < rightLayer.getLength(); i++) {
                for (int j = 0; j < leftLayer.getLength(); j++) {
                    double dotW = rightLayer.getGamma().get(u, i) * leftLayer.get(u, j);
                    weights.append(-alpha * dotW, j, i);
                }
                double dotT = rightLayer.getGamma().get(u, i);
                rightLayer.getThresholds().append(alpha * dotT, i);
            }
        }
    }

    @Override
    public void activate() {
        for (int u = 0; u < rightLayer.getMiniBatchSize(); u++) {
            for (int j = 0; j < rightLayer.getLength(); j++) {
                double value = 0.;
                for (int i = 0; i < leftLayer.getLength(); i++) {
                    value += leftLayer.get(u, i) * weights.get(i, j);
                }
                value -= rightLayer.getThresholds().get(j);
                value = functions.getActivationFunction().function(value);
                rightLayer.set(value, u ,j);
            }
        }
    }

    @Override
    public PerceptronLayer generateLayer(int... size) throws IOException {
        if (this.rightLayer != null) {
            throw new UnsupportedOperationException(
                    "FullyConnectedConn: generateLayer(): right layer in connection are" +
                    " already not null.");
        }
        if (this.leftLayer == null) {
            throw new UnsupportedOperationException(
                    "FullyConnectedConn: generateLayer(): left layer should not be null" +
                    ". You probably forgot to call 'addFirstLayr()");
        }
        PerceptronLayer rightLayer = new PerceptronLayer(size);
        this.rightLayer = rightLayer;
        rightLayer.setThresholds(new DoubleSetImpl(rightLayer.getLength()));
        readerT.read(rightLayer.getThresholds());
        this.weights = new DoubleSetImpl(this.leftLayer.getLength(), this.rightLayer.getLength());
        readerW.read(this.weights);
        return rightLayer;
    }

    @Override
    public void backpropagation() {
        for (int u = 0; u < leftLayer.getMiniBatchSize(); u++) {
            for (int i = 0; i < leftLayer.getLength(); i++) {
                double sum = 0.;
                for (int j = 0; j < rightLayer.getLength(); j++) {
                    sum += rightLayer.getGamma().get(u, j) *
                           weights.get(i, j);
                }
                leftLayer.getGamma().set(sum * functions.getDerivativeFunction().function(
                        leftLayer.get(u, i)), u, i);
            }
        }
    }
}
