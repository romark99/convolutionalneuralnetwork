package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.layer.Filter;
import com.bstu.brest.coursework.architecture.layer.MapLayer;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public abstract class ConvolutionalConn extends ConnectionChangeWeightsImpl<MapLayer, MapLayer>
        implements Backpropagational {

    protected final int poolSizeX;

    protected final int poolSizeY;

    protected int ilWidth;

    protected int ilHeight;

    protected DoubleSet ilGamma;

    /**
     * Points which were activated.
     */
    protected final ArrayList<Point> activatedPoints;

    public abstract Filter[] getFilters();

    /**
     * Class with point coordinates.
     */
    public static class Point {

        public Point(int xx, int x, int y, int z) {
            this.xx = xx;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /**
         * Coordinates.
         */
        public final int xx, x, y, z;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return xx == point.xx && x == point.x &&
                   y == point.y &&
                   z == point.z;
        }

        @Override
        public int hashCode() {
            return Objects.hash(xx, x, y, z);
        }

        int[] toIntArray() {
            return new int[]{xx, x, y, z};
        }
    }

    protected abstract void convConstructor() throws IOException;

    /**
     * Constructor without layers. SHOULD INITIALIZE THRESHOLDS.
     */
    public ConvolutionalConn(ActivationFunctions functions, int poolSizeX,
                               int poolSizeY) {
        super(ALPHA, functions);
        this.poolSizeX = poolSizeX;
        this.poolSizeY = poolSizeY;
        activatedPoints = new ArrayList<>();
    }

    @Override
    public MapLayer generateLayer(int... size) throws IOException {
        if (this.rightLayer != null) {
            throw new UnsupportedOperationException(
                    "PoolingConn: generateLayer(): right layer in connection is " +
                    "already not null.");
        }
        if (this.leftLayer == null) {
            throw new UnsupportedOperationException(
                    "ConvolutionalConn: generateLayer(): left layer should not be null." +
                    " You probably forgot to call 'addFirstLayer()");
        }
        if (size.length != 4) {
            throw new IllegalArgumentException(
                    "ConvolutionalConn: generateLayer(): size does not have 4 args.");
        }
        if (leftLayer.getWidth() < size[2] ||
            leftLayer.getHeight() < size[3]) {
            throw new IllegalArgumentException(
                    "ConvolutionalConn: generateLayer(): maps of right layer should not" +
                    " be bigger than left ones.");
        }
        ilWidth = size[2];
        ilHeight = size[3];
        ilGamma = new DoubleSetImpl(size);
        if (ilHeight % poolSizeX != 0) {
            throw new IllegalArgumentException(
                    "PoolingConn: generateLayer(): leftLayer.height % rightLayer.height" +
                    " should be equal to 0.");
        }
        if (ilWidth % poolSizeY != 0) {
            throw new IllegalArgumentException(
                    "PoolingConn: generateLayer(): leftLayer.width % rightLayer.width " +
                    "should be equal to 0.");
        }

        MapLayer rightLayer = new MapLayer(size[0],
                                           size[1],
                                           ilWidth / poolSizeX,
                                           ilHeight / poolSizeY);
        this.rightLayer = rightLayer;
        convConstructor();
        return rightLayer;
    }
}
