package com.bstu.brest.coursework.architecture.connection;

import com.bstu.brest.coursework.architecture.functions.ActivationFunction;
import com.bstu.brest.coursework.architecture.functions.ActivationFunctions;
import com.bstu.brest.coursework.architecture.layer.Filter;
import com.bstu.brest.coursework.architecture.layer.Filter2D;
import com.bstu.brest.coursework.types.DoubleSet;
import com.bstu.brest.coursework.types.DoubleSetImpl;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Convolutional connection.
 */
public class ConvolutionalConn2D extends ConvolutionalConn {

    /**
     * Array of filters.
     */
    private Filter2D[] filters;

    @Override
    public Filter[] getFilters() {
        return filters;
    }

    @Override
    public void convConstructor() throws IOException {
        filters = new Filter2D[rightLayer.getNumberOfMaps()];
        DoubleSet thresholds = new DoubleSetImpl(filters.length);
        readerT.read(thresholds);
        for (int i = 0; i < filters.length; i++) {
            filters[i] = new Filter2D(leftLayer.getWidth() - ilWidth + 1,
                                      leftLayer.getHeight() - ilHeight +
                                      1);
            readerW.read(filters[i]);
            filters[i].setThreshold(thresholds.get(i));
        }
    }

    /**
     * Constructor without layers. SHOULD INITIALIZE THRESHOLDS.
     */
    public ConvolutionalConn2D(ActivationFunctions functions, int poolSizeX,
                               int poolSizeY) {
        super(functions, poolSizeX, poolSizeY);
    }

    /**
     * Constructor without layers. SHOULD INITIALIZE THRESHOLDS.
     */
    public ConvolutionalConn2D(ActivationFunctions functions) {
        this(functions, 1, 1);
    }

    @Override
    public void activate() {
        ActivationFunction activationFunction = functions.getActivationFunction();
        activatedPoints.clear();
        for (int u = 0; u < rightLayer.getMiniBatchSize(); u++) {
            for (int i = 0; i < filters.length; i++) {
                double threshold = filters[i].getThreshold();
                ArrayList<Point> localActivatedPoints = new ArrayList<>();
                for (int xstep = 0; xstep < poolSizeX; xstep++) {
                    for (int ystep = 0; ystep < poolSizeY; ystep++) {
                        int whatPoint = 0;
                        for (int j = 0; j < ilWidth; j += poolSizeX) {
                            for (int k = 0; k < ilHeight; k += poolSizeY) {
                                double value = getActivatedValue(filters[i], u, j + xstep,
                                                                 k + ystep);
                                value = activationFunction.function(value - threshold);
                                if (xstep == 0 && ystep == 0) {
                                    rightLayer.set(value, u, i, j / poolSizeX,
                                                   k / poolSizeY);
                                    localActivatedPoints
                                            .add(new Point(u, i, j + xstep, k + ystep));
                                } else if (rightLayer.get(u, i, j / poolSizeX,
                                                          k / poolSizeY) < value) {
                                    rightLayer.set(value, u, i, j / poolSizeX,
                                                   k / poolSizeY);
                                    localActivatedPoints.set(whatPoint,
                                                             new Point(u, i, j + xstep,
                                                                       k + ystep));
                                }
                                whatPoint++;
                            }
                        }
                    }
                }
                activatedPoints.addAll(localActivatedPoints);
            }
        }
    }

    private double getActivatedValue(Filter2D filter, int u, int j, int k) {
        double value = 0.;
        for (int y = 0; y < filter.getWidth(); y++) {
            for (int z = 0; z < filter.getHeight(); z++) {
                double valueFromFilter = filter.get(y, z);
                double sum = 0.;
                for (int x = 0; x < leftLayer.getNumberOfMaps(); x++) {
                    sum += leftLayer.get(u, x, y + j, z + k);
                }
                value += valueFromFilter * sum;
            }
        }
        return value;
    }

    @Override
    public void backpropagation() {
        if (activatedPoints.size() != rightLayer.getCount()) {
            throw new UnsupportedOperationException(
                    "ConvolutionalConn: backpropagation(): number of activated points " +
                    "is wrong.");
        }
        if (activatedPoints.isEmpty()) {
            throw new UnsupportedOperationException(
                    "SHOULD BE DONE: convolutional + convolutional connections!");
        }
        ilGamma.setZeros();
        leftLayer.getGamma().setZeros();
        for (int i = 0; i < activatedPoints.size(); i++) {
            ilGamma.set(rightLayer.getGamma().getBySingleIndex(i),
                        activatedPoints.get(i).toIntArray());
            getGammas(activatedPoints.get(i));
        }
    }

    /**
     * Calculates gammas for the activated point (u, x, y, z).
     */
    private void getGammas(Point point) {
        int u = point.xx; // number of image in minibatch
        int x = point.x; // number of map
        int y = point.y; // width coordinate
        int z = point.z; // height coordinate

//        int depthOfFilter = filters[x].getDepth();
        int heightOfFilter = filters[x].getHeight();
        int widthOfFilter = filters[x].getWidth();
        double internalLayerGammaValue = ilGamma.get(u, x, y, z);
        for (int k = 0; k < leftLayer.getNumberOfMaps(); k++) {
            for (int i = y; i < y + widthOfFilter; i++) {
                for (int j = z; j < z + heightOfFilter; j++) {
                    double result = internalLayerGammaValue *
                                    functions.getDerivativeFunction()
                                            .function(leftLayer.get(u, k, i, j)) *
                                    filters[x].get(i - y, j - z);
                    leftLayer.getGamma().append(result, u, k, i, j);
                }
            }
        }
    }

    @Override
    public void changeWeightsAndThresholds() {
        if (activatedPoints.isEmpty()) {
            throw new UnsupportedOperationException(
                    "SHOULD BE DONE: convolutional + convolutional connections!");
        } else {
            for (Point activatedPoint : activatedPoints) {
                int xx = activatedPoint.xx;
                int x = activatedPoint.x;
                int y = activatedPoint.y;
                int z = activatedPoint.z;
//                int depth = filters[x].getDepth();
                int width = filters[x].getWidth();
                int height = filters[x].getHeight();
                double multiplier = alpha * ilGamma.get(xx, x, y, z);
//                             *  functions.getDerivativeFunction().function(rightLayer
//                             .get(f, y, z));
                for (int i = 0; i < width; i++) {
                    for (int j = 0; j < height; j++) {
                        double sum = 0.;
                        for (int k = 0; k < leftLayer.getNumberOfMaps(); k++) {
                            sum += leftLayer.get(xx, k, y + i, z + j);
                        }
                        filters[x].append(-multiplier * sum, i, j);
                    }
                }
                filters[x].appendToThreshold(multiplier);
            }
        }
//        System.out.println("-------------------------");
        activatedPoints.clear();
    }
}
