package com.bstu.brest.coursework.architecture.layer;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Layer with maps.
 */
public class MapLayer extends LayerWithGammas {

    /**
     * Number of maps. Layer depth.
     */
    private final int numberOfMaps;

    /**
     * Width of map
     */
    private final int width;

    /**
     * Height of map
     */
    private final int height;

    /**
     * Constructor.
     */
    public MapLayer(int... args) {
        super(args);
        if (args.length != 4) {
            throw new IllegalArgumentException("MapLayer(): args should have 4 numbers.");
        }
        numberOfMaps = args[1];
        width = args[2];
        height = args[3];
    }

    public int getNumberOfMaps() {
        return numberOfMaps;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


}
