package com.bstu.brest.coursework.architecture.functions;

import java.util.ArrayList;

/**
 * Class with additional helpful functions.
 */
public class HelpFunctions {

    /**
     * Generate random number.
     *
     * @param left  - left border.
     * @param right - right border.
     * @return - generated number.
     */
    public static Double random(Double left, Double right) {
        if (left.equals(right)) {
            return left;
        } else {
            return (right - left) * Math.random() + left;
        }
    }

    /**
     * Linear function.
     */
    public static Double linearFunction(Double sum) {
        return sum;
    }

    /**
     * Derivative of linear function.
     */
    public static Double derivativeLinearFunction(Double y) {
        return 1.;
    }

    /**
     * Sigmoid function.
     */
    public static Double sigmoidFunction(Double sum) {
        return 1. / (1. + Math.exp(-sum));
    }

    /**
     * Derivative of sigmoid function.
     */
    public static Double derivativeSigmoidFunction(Double y) {
        return y * (1. - y);
    }

    /**
     * RELU function.
     */
    public static Double reluFunction(Double sum) {
        return sum < 0. ? 0. : sum;
    }

    /**
     * Derivative of sigmoid function.
     */
    public static Double derivativeReluFunction(Double y) {
        return y < 0. ? 0. : 1.;
    }

    /**
     * Tanh function.
     */
    public static Double tanhFunction(Double sum) {
        return Math.tanh(sum);
    }

    /**
     * Derivative of tanh function.
     */
    public static Double derivativeTanhFunction(Double y) {
        return 1 - y * y;
    }

    public static Double quadraticFunction(Double output, Double etalon) {
        return 0.5 * Math.pow(output - etalon, 2.);
    }

    public static Double deltaQuadraticFunction(Double output, Double etalon,
                                                ActivationFunction derivativeFunction) {
        return (output - etalon) * derivativeFunction.function(output);
    }

    public static Double crossEntropyFucntion(Double output, Double etalon) {
        double d = -etalon * Math.log(output) - (1. - etalon) * Math.log(1. - output);
        if (Double.isInfinite(d)) {
            d = d > 0 ? Double.MAX_VALUE : -Double.MAX_VALUE;
        } else if (Double.isNaN(d)) {
            d = 0.;
        }
        return d;
    }

    public static Double deltaCrossEntropyFucntion(Double output, Double etalon,
                                                   ActivationFunction derivativeFunction) {
        return output - etalon;
    }

    public static Double cosinus(Double x) {
        return Math.cos(x);
    }
}
