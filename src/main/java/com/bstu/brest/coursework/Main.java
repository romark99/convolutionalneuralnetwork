package com.bstu.brest.coursework;

import com.bstu.brest.coursework.apps.App;
import com.bstu.brest.coursework.apps.cnn.CnnApp;
import com.bstu.brest.coursework.apps.netattacks.NetAttacksDetectionApp;

import java.io.IOException;

/**
 * Main application.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        App app = new CnnApp();
        app.main(args);
    }
}
